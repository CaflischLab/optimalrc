#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import numpy as np

from optimalRC.RC import AdaptiveReactionCoord
from optimalRC.Features import FeatureFeeder
from optimalRC.util import save_model
from optimalRC.util.log import setup_logging

setup_logging()
logger = logging.getLogger(__name__)

top = 'protein_topology.pdb'
traj = 'protein_trajectory.xtc'

f = FeatureFeeder(trajectory_file=traj,
                  topology_file=top,
                  stream=True, # if trajectory is too large to fit in memory
                  traj_length=6656000, # number of frames; can set to None if unknown
                  atom_sel='name != NA and name != CL', # passed to mdtraj.load()
                  n_features=500, # distances computed at once (memory restrictions)
                  )

# Optimization settings
adapt_interv = 200
spatial_tol = 0.03
n_iter = 1001
deg = 4
deg_r = 8
deg_adapt = 8
save_freq = 200

# Initialize optimizer
z = AdaptiveReactionCoord(adaptive_interval=adapt_interv, spatial_tol=spatial_tol)
# 0 for state A, 1 for state B, 0.5 (or random in [0, 1]) otherwise
states_AB = np.load('statesAB.npy')
# Two frames from the same trajectory are marked with the same integer
traj_indices = np.load('traj_indices.npy')
# Initialize RC
z.initialize_RC(states_AB, traj_indices=traj_indices)


def main():
    for i in range(n_iter):
        feat, desc = f()
        z(degree=deg, new_feature=feat, description=desc,
          optimize_r_degree=deg_r, adapt_degree=deg_adapt)

        if i%(save_freq) == 0 and i != 0:
            save_model(f'optimized_{i}_steps.pickle', z)
            logger.info('Saved model.')
    return

if __name__ == '__main__':
    main()
