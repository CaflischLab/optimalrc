#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  7 15:35:46 2022

@author: jwidmer
"""
import logging
import numpy as np
logger = logging.getLogger(__name__)
"""Implementation of WE-reweighting of PIGS trajectories as proposed in
https://doi.org/10.1063/1.5063556"""


def calc_zscore(features: np.array, n_frames: int, n_reps: int) -> np.ndarray:
    """Z-scores a feature matrix of shape (n_frames, n_reps, n_features)
    by (features - mu) / sigma to loose the scale of each feature."""
    # save shape
    s = features.shape
    # Need input shape: (n_frames, n_reps, n_features)
    f = features.reshape(n_frames, n_reps, -1)
    # Want output shape: (n_features), so take mean over n_reps and n_frames
    mu = f.mean(axis=(1, 0))
    sigma = f.std(axis=(1, 0))
    if 0. in sigma:
        raise ZeroDivisionError(f'Variance in columns {np.where(sigma == 0)[0]} is 0')
    return ((f - mu) / sigma).reshape(s)


def pnorm_distance(feature: np.array, p: int) -> int:
    """Takes in a feature matrix of dimension (n_rep, d). Calculates
    the pairwise p-norm of each d-dimensional feature to the p-th power
    (i.e., the \sum_k (v_i_k - v_j_k)^p )
    and returns the selected column.
    Note that p-norms are monotonous with respect to p."""
    assert len(feature.shape) == 2
    out = feature[:, None] - feature
    distance_matrix = np.linalg.norm(out, ord=p, axis=2) ** p
    return distance_matrix


def periodic_distance(feature: np.array, p: int) -> int:
    """Takes in a feature matrix of angles in radtians with dimension (n_rep, d).
    Calculates the smallest pairwise p-norm along a circle of each d-dimensional
    feature and returns the selected column.
    Note that p-norms are monotonous with respect to p."""
    assert len(feature.shape) == 2
    # Make sure it is in radians
    assert feature.max() <= 2*np.pi and feature.min() >= -np.pi
    out = (feature[:, None] - feature) % (2*np.pi)
    distance_matrix = np.linalg.norm(out, ord=p, axis=2)
    # Select the smaller distance along the circle
    xx = distance_matrix >= distance_matrix.T
    distance_matrix[xx] = distance_matrix.T[xx]
    return distance_matrix


def nearest_neighbours(n_distances: np.ndarray, **kwargs) -> np.ndarray:
    """Splits weights uniformly between the n potential sinks.
    Distances are ignored."""
    n = n_distances.shape[0]
    sink_weight = np.ones_like(n_distances)/n
    return sink_weight


def RBF(n_distances: np.ndarray, gamma: float) -> np.ndarray:
    """Computes the RBF-kernel exp(-gamma * d^p), where gamma is 1/2*\sigma^2.
    The distance to the terminated replica is precomputed, therefore, the n
    closest distances are transformed by exp(-gamma * d^p) to a similarity.
    The higher gamma, the more weight is assigned to distant points.

    For gamma = 0, weights will correspond to splitting evenly between
    n nearest-neighbours.
    For gamma = \inf, weights will correspond focussing all the weight on
    the nearest neighbour.
    
    If the p-norm was calculated using p=1, the kernel corresponds to an exponential
    kernel.
    If p=2, the kernel corersponds to an RBF-kernel.

    gamma, float
        Length-scale parameter of the kernel. Must be strictly positive."""
    assert gamma > 0.
    K = np.exp(-gamma * n_distances)
    if K.sum() == 0.:
        raise ZeroDivisionError(f'Sinks are too distant from terminated replica. \
All similarities are 0. Try choosing gamma smaller than current value of {gamma}.')
    sink_weight = K / K.sum()
    return sink_weight


def absorb_weights(weights: np.ndarray, replicas_terminated: np.ndarray,
                   untouched_replicas: np.ndarray, feature: np.ndarray,
                   p: int, n: int, kernel: callable = nearest_neighbours,
                   verbose: bool = False, **kernel_kwargs) -> np.array:
    """At a specific reseeding, loop over the replicas that were terminated
    and need to have their weight reassigned.
    First, a feature matrix is used to rank replicas by distance (p-norm)
    to the terminated one. Then, it is ensured that only replicas that 
    were not involved in reseeding are considered.
    Finally, the similarities of the n closest replicas are normalized to
    determine the fraction of weight absorbed by each of the n replicas.

    If n=1, the most similar replica absorbs the full weight of the terminated
    trajectory.
    If n=\inf and a degenerate feature (the same for all replicas) is supplied,
    the weight is split evenly between all untouched replicas.

    Returns the updated weight-vector."""
    # The replica that is replaced
    for terminated in replicas_terminated:
        # Rank replicas by distance to the terminated one in terms of the feature
        A = pnorm_distance(feature, p=p)
        # Find the replica that has minimum distance from the terminated one
        distances = A[terminated, :]
        # Sort by distance (shortest first)
        ranked_replicas = np.argsort(distances)
        # Find the untouched replicas among all replicas sorted by distance
        sink_ids = [rep for rep in ranked_replicas
                    if rep in untouched_replicas][:n]
        if len(sink_ids) == 0:
            raise ValueError("No similar untouched replicas found.")
        # Warn if this filtering results in fewer than n sink replicas
        elif len(sink_ids) < n:
            cannot_use = np.setdiff1d(ranked_replicas, sink_ids)
            logger.info(f'WARNING: Replicas {cannot_use} are involved in \
reseeding and are not used as weight sinks. Using {len(sink_ids)} sinks \
instead of {n}.')
        n_distances = distances[sink_ids]
        sink_weight = kernel(n_distances, **kernel_kwargs)
        # Reassign weight to the n most similar ones according to their weight
        new_weights = np.ones_like(sink_ids) * weights[terminated] * sink_weight
        # logger.info(f"{new_weights}")
        if verbose:
            logger.info(f"Replicas {sink_ids} absorb weight from replica {terminated}.")
            logger.info("Features:")
            logger.info(f"Absorbing: {feature[sink_ids]} \
\nTerminated: {feature[terminated]}") 
        weights[sink_ids] += new_weights
        
    return weights


def split_weights(weights: np.ndarray, replicas_duplicated: np.ndarray,
                  reseeding: np.ndarray, verbose: bool = False) -> np.array:
    """At a specific reseeding, redistribute the current weight of the
    multiplied replica evenly among its copies.
    
    Returns the updated weight-vector."""
    # The replica that is multiplied
    for duplicated in replicas_duplicated:
        # The replicas that are continued from this configuration
        offspring = np.where(reseeding == duplicated)[0]
        if verbose:
            logger.info(f"Replica {duplicated} split among replicas {offspring}.")
        # Distribute weight of duplicated replica among its copies
        weights[offspring] = weights[duplicated]/offspring.shape[0]
    # logger.info(f'Splitting: {weights}')
    return weights


def find_weights(trace: np.ndarray, features: np.ndarray,
                 n: int = 1, p: int = 1, zscore: bool = False,
                 kernel: callable = nearest_neighbours, verbose: bool = False,
                 distancef: callable = pnorm_distance,
                 **kernel_kwargs) -> np.ndarray:
    """
    At every reseeding, redistribute the weight of 
   
    i) terminated replicas, and
    ii) duplicated replicas.
                
    using information from the tracefile N_000_PIGSTRACE.dat and some features
    that compute kinetic proximity.
    It is crucial that the tracefile is converted to 0-indexing.

    First, for each terminated replica, find the n closest replicas
    given some feature vector, a distance, and a kernel on that distance to
    translate distance to similarities. These closest replicas absorb
    the weight of the terminated replica weighted by the normalized similarity
    to the terminated replica. The parameter p is an exponent applied to the
    distance between replica and serves to convert the scale of the
    geometric features to the scale of kinetic similarity.

    Then, find the replicas that are used for reseeding.
    If replica i is reseeded k times, the weights of its offspring are updated
    to be w(i)/k, where w(i) is the current weight of trajectory i.

    If n=1, the closest replica absorbs the full weight.
    If n=\inf and a degenerate feature (the same for all replicas) is supplied,
    the weight is split evenly between all untouched replicas.

    Return the matrix that contains the vector of weights for every replica
    at each reseeding event. Additionally, it contains a uniform distribution
    in the first row (the weight prior to the first reseeding).

    When using multiple features for finding probability sinks, the distance
    in the feature space can be dominated by the different scale of the features.
    If this is not desirable, pass `z_score=True`.
    
    The boolean `verbose` prints detailed information on reseeding if enabled,
    which can slow down the function.
    """
    if (trace.shape[0] != features.shape[0]) or (trace.shape[1] != features.shape[1]):
        raise ValueError('Features and trace must have compatible dimensions.')

    logger.info(f'Distance between replicas is measured using "{distancef.__name__}". \
Using kernel: "{kernel.__name__}" to lump weight to {n} replicas \
using distance to the power {p}. Z-scoring is turned {["OFF", "ON"][int(zscore)]}. \
Kernel parameters: {kernel_kwargs}')
    n_frames = trace.shape[0]
    n_reps = trace.shape[1]
    logger.info(f'Assuming {n_reps} replicas with {n_frames} frames each.')
    all_replicas = np.arange(n_reps)
    # Optionally: z-score
    if zscore == True:
        features = calc_zscore(features, n_reps, n_frames)
    
    # Initialize weights to uniform. 
    weight_matrix = np.zeros((trace.shape[0] + 1, trace.shape[1]))
    weight_matrix[0] = 1/n_reps
    for i, (reseeding, feature) in enumerate(zip(trace, features)):
        w = np.copy(weight_matrix[i])
        # Find all replicas that are multiplied
        replicas, counts = np.unique(reseeding, return_counts=True)
        replicas_duplicated = replicas[counts > 1]
        # Find all replicas that are replaced
        replicas_terminated = np.setdiff1d(all_replicas, replicas)
        # If there is no reseeding, just copy the previous probabilities
        if replicas_terminated.size == 0:
            weight_matrix[i+1] = w
            continue
        # Make sure empty intersection between duplicated and terminated
        if np.intersect1d(replicas_duplicated, replicas_terminated).size != 0:
            raise ValueError(f'Reseeding number {i+1}: Some terminated replicas \
appear also duplicated. Corrupt trace?')
        # Find replicas that were not terminated
        untouched_replicas = np.setdiff1d(all_replicas, replicas_terminated)
        # Each replica has a vector of features describing it at reseeding,
        # so each feature is a matrix from (n_reps, d)
        # Use only replicas that are untouched to absorb the weight;
        # Therefore, take only a subset of the features
        feature = feature.reshape(n_reps, -1)
        # Transfer weight from terminated replicas to the n most similar ones
        if verbose:
            logger.info("===========================================")
            logger.info(f"Reseeding number {i+1}:")
        # using the p-norm of the feature
        w = absorb_weights(w, replicas_terminated, untouched_replicas, feature,
                           p, n, kernel, verbose, **kernel_kwargs)
        if verbose:
            logger.info("-------------------------------------------")
        # Split the weights of the duplicated replicas to their children
        w = split_weights(w, replicas_duplicated, reseeding)

        # Weight should sum to 1 after every reseeding
        if np.allclose(1., w.sum()):
            weight_matrix[i+1] = w
        else:
            msg = f"FAILED AT ITERATION {i}. Probability sums to {w.sum():.3f}"
            logger.info(msg)
            raise ValueError(msg)
    return weight_matrix

