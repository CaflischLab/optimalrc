#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 21:20:24 2021

@author: julian
"""
import logging
import math
import numpy as np
import tensorflow as tf
from typing import List, Tuple
from .constants import K_BOLTZMANN
from .BasisFunctions import basis_poly_r, basis_poly_ry, laplace_distribution
logger = logging.getLogger(__name__)

class ReactionCoord:
    """Contains a reaction coordinate for the transition between boundary
    states A and B and optimizes said coordinate to approximate the committor.
    """

    def __init__(self):

        self.traj_length = None
        # Partitioning of trajectory
        self.state_A = None
        self.state_B = None
        self.N_a_to_b, self.N_b_to_a, self.n_AB = None, None, None
        # Two frames from the same trajectory are marked with the same integer
        self.traj_indices = None
        # traj_breaks(i)=1 if X(i) and X(i+1) belong to the same trajectory
        self.traj_breaks = None
        self.traj_weights = None

        # Progress of optimization & parameters
        self.Dt = 1
        self.r = None
        self.ri_is_boundary = None
        self.weights = None # weights to reweight r to equilibrium; reset when r is optimized
        self.weights_optimization_steps = 0 # optimization steps; reset when r is optimized

        self.RC_optimization_steps = 0
        self.Dr_squared = None
        self.selected_features_descr = None



    def __call__(self, **kwargs):
        """Calls ReactionCoord.update_reaction_coord to optimize the reaction coordinate.
        kwargs to specify 'degree' of polynomial."""
        return self.optimize(**kwargs)


    def __repr__(self):
        return f"{self.__class__}()"


    def __str__(self):
        return f'{self.__class__}\n{self.RC_optimization_steps} optimization steps,\
 Dr^2 = {self.get_Dr_squared():.3f}'


    def __eq__(self, other):
        """Two instances of this class are considered equal if their coordinate
        r is equal and underwent the same amount of optimization."""
        if isinstance(other, ReactionCoord):
            try:
                return (self.r == other.r and
                        self.RC_optimization_steps == other.RC_optimization_steps)
            except ValueError:
                return ((self.r == other.r).all() and
                        self.RC_optimization_steps == other.RC_optimization_steps)
        return False


    def initialize_RC(self, states_AB: np.ndarray,
                      traj_indices: np.ndarray = None,
                      traj_weights: np.ndarray = None) -> None:
        """
        states_AB
            Provide states A and B each as non-intersecting indices of frames
            in the whole trajectory.
            states_AB needs to be a 1D time series where A = 0, B = 1,
            and in (0, 1) elsewhere. Note: Can also supply a pre-optimized
            coordinate r to continue optimization.
        traj_indices
            Time series assigning each continuous segment on the
            trajectory underlying the RC a unique integer. Defaults to a single
            long trajectory.
        traj_weights
            Time series that assigns to each frame the statistical weight of
            the continuous segment it is part of (see traj_indices).
        """
        self.r = states_AB
        self.traj_length = self.r.shape[0]

        if traj_indices is None:
            traj_indices = np.ones(self.traj_length, np.float64)
        # Check that it's an array of right shape
        elif np.array(traj_indices).shape != (self.traj_length, ):
            err = f'"traj_indices" needs to be an array of shape {(self.traj_length, )}'
            logger.exception(err)
            raise ValueError(err)
            
        traj_breaks = np.ones_like(traj_indices, np.float64)
        traj_breaks[traj_indices != np.roll(traj_indices, -1)] = 0 

        self.traj_indices = np.array(traj_indices, np.float64)
        self.traj_breaks = traj_breaks
        if traj_weights is None:
            traj_weights = np.ones_like(self.r)
        else:
            if np.array(traj_weights).shape != (self.traj_length, ):
                err = f'"traj_indices" needs to be an array of shape {(self.traj_length, )}'
                logger.exception(err)
                raise ValueError(err)
            logger.info(r'Using frame-wise weights to minimize Dr^2')
        self.traj_weights = traj_weights

        self.Dr_squared = list()
        # Frame indices where the trajectory is in state A or B, respectively
        self.state_A = np.where(states_AB == 0.)[0]
        self.state_B = np.where(states_AB == 1.)[0]
        # Evaluate initial Dr^2
        Dr2 = self.evaluate_Dr2(states_AB, traj_breaks)
        self.Dr_squared.append(Dr2)
        self.ri_is_boundary = np.zeros((1, self.traj_length))[0]
        self.ri_is_boundary[self.state_A] = 1
        self.ri_is_boundary[self.state_B] = 1

        logger.info(f'Defined states.\nA: {len(self.state_A)} instances\
\nB: {len(self.state_B)} instances.')

        self.N_a_to_b, self.N_b_to_a, self.n_AB = self.count_transitions_AB(states_AB,
                                                                            traj_indices)
        self.RC_optimization_steps = 0
        # Reset weights when r is changed
        self.weights = tf.ones_like(traj_indices, np.float64)
        logger.info('Initialized weights to 1.')
        self.weights_optimization_steps = 0
        logger.info(f'Observed transitions:\n\
{self.N_a_to_b} A -> B,\n{self.N_b_to_a} B -> A,\n{self.n_AB} total.')
        logger.info(f'Assumed trajectory length: {len(states_AB)} snapshots.')
        logger.info(f'Individual trajectories: {len(set(traj_indices))}.')



    @staticmethod
    def count_transitions_AB(states_AB: np.ndarray,
                             traj_indices: np.ndarray = None) -> int:
        """Count transitions A -> B"""
        N_a_to_b = 0
        N_b_to_a = 0
        if traj_indices is None:
            traj_indices = np.ones(states_AB.shape, np.float64)
        for itraj in np.unique(traj_indices):
            # Filter frames only in one consecutive trajectories
            active = states_AB[np.where(traj_indices == itraj)]
            # Remove intermediate states, i.e. states not in A or B
            boundary_states = active[(active == 0) | (active == 1)]
            # counts of values [-1, 0, 1] in np.diff
            unique, counts = np.unique(np.diff(boundary_states), return_counts=True)
            # Make dict to enable default values via .get method
            counts = dict(zip(unique, counts))
            N_b_to_a += counts.get(-1, 0) # How many diff = -1 (B=1 -> A=0)
            N_a_to_b += counts.get(1, 0) # How many diff = 1 (A=0 -> B=1)

        N_AB = N_a_to_b + N_b_to_a
        return N_a_to_b, N_b_to_a, N_AB


    def optimize(self,
                 degree: int = None,
                 new_feature: np.ndarray = None,
                 description: str = None,
                 optimize_r_degree: int = None) -> None:
        """Takes array new_feature to update reaction coordinate r to update
        r as r' = f(r, new_feature), where f is a 2D polynomial of the specified
        degree (default: 4 terms). The total squared deviation over the trajectory
        is calculated on the updated reaction coordinate, r'."""
        
        if not np.any(self.ri_is_boundary):
            err = 'States A and B must be defined by passing \
an array of indices marking A and B to ReactionCoord.initialize_RC()'
            logger.exception(err)
            raise ValueError(err)
        if self.RC_optimization_steps == 0:
            logger.info(f'Optimization initiated using parameters:\
 {self.__class__}: degree={degree}, optimize_r_degree={optimize_r_degree}')
        updated_r = self.r
        # Optionally update r with a new feature
        if degree is not None:
            updated_r = self._optimize_ry(updated_r, degree,
                                          new_feature, description)
        # Optionally update r without including a new feature
        if optimize_r_degree is not None:
            updated_r = self._optimize_r(updated_r, optimize_r_degree) 

        self.r = updated_r
        self.RC_optimization_steps += 1
        Dr_squared = self.evaluate_Dr2(self.r, self.traj_breaks)
        self.Dr_squared.append(Dr_squared)
        # Remove weights when r is changed
        self.weights = tf.ones_like(self.r, np.float64)
        self.weights_optimization_steps = 0



    def _optimize_ry(self, updated_r: np.ndarray, degree: int,
                     new_feature: np.ndarray, description: np.ndarray,
                     fenv: np.ndarray = None) -> np.ndarray:
        # Check if feature has the right shape and add the description
        new_feature = self.add_feature(new_feature, description)
        # Calculate polynomial basis functions of r and y of given degree
        basis_functions = basis_poly_ry(self.r, new_feature,
                                        degree=degree, fenv=fenv)
        # Use optimal coefficients with given basis functions to find r
        # that minimizes Dr^2
        basis_fn_b, coeffs = self.calculate_optimal_coeffs(self.r,
                                                           basis_functions,
                                                           self.ri_is_boundary,
                                                           (self.traj_breaks*
                                                           self.traj_weights))
        updated_r = self.update_r(self.r, basis_fn_b, coeffs)
        return updated_r


    def _optimize_r(self, updated_r: np.ndarray,
                    degree: int = None, fenv: np.ndarray = None) -> np.ndarray:
        basis_functions = basis_poly_r(self.r, degree=degree, fenv=fenv)
        basis_fn_b, coeffs = self.calculate_optimal_coeffs(updated_r,
                                                           basis_functions,
                                                           self.ri_is_boundary,
                                                           (self.traj_breaks*
                                                           self.traj_weights))
        updated_r = self.update_r(updated_r, basis_fn_b, coeffs)
        return updated_r

    
    def optimize_weights(self, degree: int = 5):
        """Reweight snapshots to equilibrium by
        w(i)\sum_j n(j|i) = \sum_j w(j) n(i|j). Weight of i times all the out-
        going transitions from i is equal to the sum """
        # initialize weights
        basis_fn = basis_poly_ry(self.weights, self.r, degree)
        coeffs = self.calculate_weight_coeffs(self.weights,
                                              basis_fn,
                                              self.traj_breaks)
        updated_weight = self.weights + tf.tensordot(coeffs, basis_fn, 1)

        self.weights = updated_weight
        self.weights_optimization_steps += 1


    @staticmethod
    @tf.function
    def update_r(r: np.ndarray,
                 fk: tf.Tensor,
                 coeffs: np.ndarray) -> np.ndarray:
        """Add linear combination of basis functions with their coefficient to r"""
        rn = r + tf.tensordot(coeffs, fk, 1)
        # updated RC is clipped to be in [0, 1]
        rn = tf.clip_by_value(rn, 0, 1)
        return rn

    
    @staticmethod
    @tf.function
    def calculate_optimal_coeffs(r: tf.Tensor,
                                 fk: tf.Tensor,
                                 ri_is_boundary: np.ndarray,
                                 traj_breaks: np.ndarray,
                                 ) -> Tuple[tf.Tensor, tf.Tensor]:
        """ implements NPq (non-parametric committor optimization) iteration.

        r 
            Time-series of the current RC estimate
        fk
            Basis functions of the variation delta r
        traj_breaks
            Trajectory indictor function:
            traj_breaks(i)=w_i if r[i] and r[i+1] belong to the same short trajectory
            and 0 otherwise. The float w_i gives the snapshot weight at r[i]
            and defaults to 1 if no weights are supplied.
            traj_breaks prevents contribution of frames not from same trajectory.
        ri_is_boundary
            Boundary indicator function:
            ri_is_boundary(i)=1 when r[i] belongs to the boundary states
            and 0 otherwise
        """
        # replacement for partitioning trajectory; selects only frames that
        # do not belong to boundary conditions and are free to be changed
        fk = fk * (1 - ri_is_boundary)
        # Calculate fk[i+1] - fk[i]
        delta_fk = fk - tf.roll(fk, shift=-1, axis=1)
        # Matrix A from https://pubs.acs.org/doi/pdf/10.1021/acs.jctc.1c00218,
        # equation (12b)
        Akj = tf.tensordot(fk*traj_breaks, delta_fk, axes=[1, 1])
        # Calculate r[i] - r[i+1]
        delta_r = tf.roll(r, shift=-1, axis=0) - r
        # Calculate b from Ax = b
        b = tf.tensordot(fk, delta_r*traj_breaks, 1)
        b = tf.reshape(b, [b.shape[0], 1])
        # Compute minimum-norm least squares solution of Ax = b
        # to find optimal coeffs minimizing Dr^2;
        # condition number tends to be high despite A being symmetric in principle
        al_j = tf.linalg.lstsq(Akj, b, fast=False)
        al_j = tf.reshape(al_j, [al_j.shape[0]])
        return fk, al_j
    
    
    @staticmethod
    @tf.function
    def calculate_weight_coeffs(r: tf.Tensor, fk: tf.Tensor,
                                traj_breaks: np.ndarray) -> tf.Tensor:
        """Implements NPNEw (non-parametric non-equilbrium re-weighting factors 
        optimization) iteration.
        
        r is the putative RC time-series
        fk are the basis functions of the variation delta r
        traj_breaks is the trajectory indictor function:
            traj_breaks(i)=1 if X(i) and X(i+1) belong to the same short trajectory
            traj_breaks prevents contribution of frames not from same trajectory.
        """

        # \Delta fk = fk(t + dt) - fk(t)
        dfk = fk - tf.roll(fk, -1, 1)
        # bk = - \sum_t \Delta fk * r; trajbreaks kills cross terms
        b = - tf.tensordot(dfk, r*traj_breaks, 1)
        b = tf.reshape(b, [b.shape[0], 1])
        scale = tf.math.reduce_sum(1 - r * traj_breaks)
        scale = tf.reshape(scale, [1, 1])
        b = tf.concat((b, scale), 0)
        # scale to make sum to 1?

        ones = tf.reshape(traj_breaks, [1, traj_breaks.shape[0]])
        # \Delta fk = fk(t + dt) - fk(t); kill cross terms, append ones
        dfk = tf.concat((dfk * traj_breaks, ones), axis=0)
        # A_kj = \sum_t \Delta fk * fj * traj_breaks;
        akj = tf.tensordot(dfk, fk, axes=[1, 1])
        # Parameters: Solve A_kj \dot al_j = b
        al_j = tf.linalg.lstsq(akj, b, fast=False)
        al_j = tf.reshape(al_j, [al_j.shape[0]])

        return al_j


    @staticmethod
    def evaluate_Dr2(r: tf.Tensor, traj_breaks: np.ndarray = None) -> float:
        """Evaluate the total squared deviation over the trajectory.
        While boundaries are not included in calculation of optimal coefficients,
        which minimize displacement upon the RC updated with and internal DOF,
        Dr^2 does include them, leading to a larger value."""
        # Equivalent to: sum([(r[k + 1] - r[k])**2 for k in range(len(r) - 1)])
        # Only exactly equivalent technically if r[0] == r[-1];
        # difference is negligible in practice
        if traj_breaks is None:
            traj_breaks = np.ones_like(r, np.float64)
        dr = r - tf.roll(r, -1, 0)
        Dr_squared = tf.tensordot(dr*traj_breaks, dr, 1).numpy()

        return Dr_squared


    @staticmethod
    def compute_Z_h(r: tf.Tensor, weights: List[float] = None,
                    dx: int = 0.1) -> Tuple[List[float], List[float]]:
        """ compute Zh, histogram-based partition function/probability
        r - list of coordinates, e.g., a trajectory time-series
        dx - bin size
        weights - list of weights, for re-weighting.
        """
        # Almost the same as np.histogram; instead of bin edges, the first
        # return argument are the floored values of r_i, the second argument
        # are the statistical weight; could replace at some point by
        # selecting bin_edges[0], bin_edges[-1], and then all the midpoints of the bin
        # edges in between from:
        # np.histogram(r, bins=np.arange(min(r), max(r) + binwidth, binwidth))
        if weights is None:
            weights = np.ones_like(r)

        zh = dict()
        for x, w in zip(r, weights):
            # x divided by binsize rounded up to next higher integer * dx
            x = math.floor(x/dx + 0.5) * dx
            # Add a count to the value in the dictionary
            zh[x] = zh.get(x, 0) + w

        # convert dictionary to two lists, sorted by coordinate r
        r = list(zh.keys())
        r.sort()
        Z_h = [zh[x] / dx for x in r]

        return r, Z_h


    @staticmethod
    def compute_Z_ca(r: List[float], a: int, traj_indices: List[int] = None,
                     weights: List[float] = None, dt: int = 1, strict: bool = False,
                     dx: float = 2e-3, mindx: float = 1e-3,
                     eq: bool = True) -> Tuple[List[float], List[float]]:
        """Computes cut-based free energy profiles Z_C,a for non-equilbrium and equilbrium cases
        non-equilbrium case (eq=False) computes only outgoing part of the Z_C,a,
        which for a=1 gives Z_q - the non-equilbrium committor criterion
        equilbrium case (eq=True), computes outgoing and ingoing parts of Z_C,a
        can be used with a single trajectory and with ensamble of trajectories
        accepts re-weighting factors

        r - list of coordinates, a trajectory time-series
        a  - exponent of the distance: (|xi-xj|)^a
        itraj - trajectory index time-series:
            tells to which short trajectory point X(i) belongs to
            if itraj=[], a single long trajectory is assumed
        weights - list of weights or re-weighting factors
        dt - stepsize
        strict - whether to construct the exact profile,
            accurate representation of step functions
        dx - bin size for coarse-graining
        mindx - minimal value of dx:
            used to suppress errors for a<0
        eq - flag to compute equilibrium profile (eq=True), by including
            in-going transitions
        """
        if weights is None:
            weights = list()
        if traj_indices is None:
            traj_indices = list()

        dzc = dict()
        tmax = len(r) - dt
        for i in range(tmax):
            # Skip iteration if r(i*dt) and r(i*dt+dt) are marked as belonging
            # to different trajectories
            if len(traj_indices) > 0 and traj_indices[i] != traj_indices[i+dt]:
                continue

            startx, x = (r[i], r[i+dt])

            if dx is not None and not strict: 
                # Rounds x to the nearest integer
                x = math.floor(x/dx + 0.5) * dx
                startx = math.floor(startx/dx + 0.5) * dx
            # Absolute displacement going from r(i*dt) to r(i*dt+dt)
            d = abs(x - startx)
            # assign d the value of mindx if d is smaller than d
            if a < 0:
                d = max(d, mindx)
            d = float(d)**a

            # Apply weight to d
            if any(weights):
                d = d * weights[i]    
            # Update coordinate value in Dict by absolute displacement d
            if startx < x:
                dzc[startx] = dzc.get(startx, 0) + d
                # Only update x if trajectory is not at equilibrium (Zq vs Zc1);
                # updating means including incoming trajectories into the sum
                if eq:
                    dzc[x] = dzc.get(x, 0) - d
            else:  
                dzc[startx] = dzc.get(startx, 0) - d
                if eq:
                    dzc[x] = dzc.get(x, 0) + d

        # Create lists sorted by coordinate value
        keys = list(dzc.keys())
        keys.sort()
        r_sorted = []
        Zc1 = []
        z = 0

        if eq:
            scale = 1/dt/2
        else:
            scale = 1/dt

        for x in keys:
            r_sorted.append(x)
            Zc1.append(float(z))
            z = z + dzc[x] * scale
            if strict:
                r_sorted.append(x)
                Zc1.append(float(z))
        # ly[0] equals 0, but a non-zero value is more convenient for further analysis
        if not strict and len(Zc1) > 1: 
            return r_sorted[1:], Zc1[1:] 
        return r_sorted, Zc1


    @staticmethod
    def compute_Z_q(**kwargs):
        """Non-equilibrium cut function"""
        # Set equilibrium to False, since explitictly Z_q is called
        kwargs['eq'] = False
        # Not sure if it can also be a != 1
        kwargs['a'] = 1
        return ReactionCoord.compute_Z_ca(**kwargs)


    @staticmethod
    def compute_Zx_multi_Dt(q: np.ndarray, Dt_list: List[int], alpha: int,
                            **kwargs):
        """Calculate Zx profiles for multiple time steps provided by Dt_list."""
        dt_to_Zca = dict()
        for dt in Dt_list:
            x, zca = ReactionCoord.compute_Z_ca(q, a=alpha, dt=dt, **kwargs)
            # Significant change only for very short trajectories compared to dt
            scale = (q.shape[0] - dt) / (q.shape[0]-1)
            zca = [zca_i / scale for zca_i in zca]
            dt_to_Zca[dt] = (x, zca)

        return dt_to_Zca


    def get_Dr_squared(self, idx: int = None, window: int = 1) -> float:
        """Returns the total squared deviation based on index idx. Returns
        the last value if no index is specified.
        If window = n > 1, the mean value over n-1 values preceding idx
        and idx is returned"""
        dr2 = self.Dr_squared
        if idx is None:
            try:
                idx = len(dr2)
            except TypeError:
                err = 'Reaction coordinate has not been initialized; \
Dr^2 is not defined.'
                logger.exception(err)
                raise ValueError(err)
        if idx < window:
            return np.mean(dr2[:idx])
        elif idx == len(dr2):
            return np.mean(dr2[idx - window: idx])
        else: # Add one because idx counts for one
            return np.mean(dr2[idx - window + 1: idx + 1])


    def get_selected_features(self, idx: int = None) -> Tuple[str, np.ndarray]:
        """Returns the description of selected features used for optimizing the reaction coordinate
        based on index idx. idx can be a integer or a list of integers.
        Returns the last value if no index is specified."""
        if idx is None:
            idx = -1

        return self.selected_features_descr[idx]


    def add_feature(self, new_feature: np.ndarray = None,
                    description: str = None) -> np.ndarray:
        """Adds a new feature to the list of used features. If no new_feature
        is provided"""
        # Initialize list recording added features
        if self.selected_features_descr is None:
            self.selected_features_descr = list()

        # Check that it's an array of right shape
        elif new_feature.shape != (self.traj_length, ):
            err = f'Feature needs to be an array of shape {(self.traj_length, )}'
            logger.exception(err)
            raise ValueError(err)

        else:
            self.selected_features_descr.append(description)

        return new_feature


    @staticmethod
    def to_natural(q: np.array, dx: float, dtsim: float = 1.,
                   traj_indices: List[int] = None, weights: List[float] = None,
                   fixZhA: bool = True, zcm1: bool = False) -> np.ndarray:
        """ transforms putative commitor coordinate to the natural coordinate,
        where diffusion coefficient is constant. The following equations are used
        Zc1=dt*D*Zh (for small dt such that FEP~const)
        D=Zc1/(Zh*dt)
        dy/dx=D^-1/2

        q - committor time-series
        dx - bin size to compute the profiles and integrate
        dtsim - the trajectory/simulation sampling interval in time units in which D=1.
        itraj - trajectory index time-series:
            tells to which short trajectory point X(i) belongs to
            if itraj=None, a single long trajectory is assumed  
        weights - list of weights, for re-weighting,
            if lw=[], no re-weighting
        fixZhA - the leftmost value of Z_H, Z_H for state A, usually has a very large value,
            which leads to unphysical results. fixZhA=True assigns it to the next value 
        zcm1 - whether to use Z_C,-1 to estimate Z_H. Z_C,-1 gives a more robust estimate
            of Z_H if the sampling interval is sufficiently small.
        """
        if traj_indices is None:
            traj_indices = list()

        if zcm1:
            _, lzcm1 = ReactionCoord.compute_Z_ca(q, a=-1, traj_indices=traj_indices,
                                              dx=dx, dt=1, weights=weights, eq=True)
            lzh=[2*x for x in lzcm1]
        else:
            _, lzh = ReactionCoord.compute_Z_h(q, weights=weights, dx=dx)
        lx1, lzc1 = ReactionCoord.compute_Z_ca(q, a=1, traj_indices=traj_indices,
                                           dx=dx, dt=1, weights=weights, eq=True)
        # If boundary had imbalanced population
        if fixZhA:
            lzh[0] = lzh[1]

        x2y = dict()
        dydx = dict()

        xl, y, x2y[xl] = (0, 0, 0)

        for x, zh, zc1 in zip(lx1, lzh, lzc1):
            dydx[xl] = (zh * dtsim/zc1)**0.5 # = D^-1/2
            y = y + dydx[xl] * (x-xl)
            x2y[x] = y
            xl = x

        dydx[1.0] = 0
        x2y[1.0] = y
        q_natural = []
        for q_i in q:
            q0 = math.floor(q_i/dx + 0.5) * dx
            # Sometimes, q0 is rounded in a way that then does not allow
            # getting the value from the dict
            try:
                qn = x2y[q0] + (q_i - q0) * dydx[q0]
            except Exception as e:
                logger.info(e, q_i)
            q_natural.append(qn)

        return np.array(q_natural)


    @staticmethod
    def compute_FEP(r: List[float], T: int = 300, **kwargs)-> Tuple[np.ndarray]:
        """
        Computes Z_H aka probabilities in bins
        bin_size gives size of bins for calculation of Z_H
        r is the reaction coordinate; ideally the committor with D = 1
        T temperature in Kelvin."""
        KbT = K_BOLTZMANN * T
        # Z_H = Z_c,-1 * 2
        x, P = ReactionCoord.compute_Z_ca(r, a=-1, **kwargs)
        FEP = -KbT * np.log(np.array(P) * 2) 
        return x, FEP
    
    
    @staticmethod
    def avg_MSD(rc: np.ndarray, timesteps: List[int]):
        """Calculate average mean-squared displacement along a 1D reaction
        coordinate a at integer timesteps. An average value is calculated over
        the possible starting frames."""
        avg_drs = [0] * len(timesteps)
        for i, dt in enumerate(timesteps):
            dt_msd = [0] * dt
            for starting_frame in range(dt):
                r = rc[starting_frame::dt]
                # skip last to omit a[-1] - a[0]
                dr = (r - np.roll(r, -1, 0))[:-1]
                avg_dr = np.dot(dr, dr) / dr.shape[0]
                dt_msd[starting_frame] = avg_dr
                
            avg_drs[i] = np.nanmean(dt_msd)
        return timesteps, avg_drs



class AdaptiveReactionCoord(ReactionCoord):
    """Committor optimization with spatial and temporal adaptivity."""
    
    def __init__(self, Dt_0: int = 1, adaptive_interval: int = 400,
                 spatial_tol: float = 0.03, adapt_scale: float = None,
                 time_adaptive_interval: int = 0, eq: bool = True,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.time_adaptive = time_adaptive_interval
        self.periodic_boundaries = False

        self.Dt = Dt_0
        self.adaptive_interval = adaptive_interval
        self.adapt_frq = adaptive_interval/10
        self.spatial_tol = spatial_tol
        self.adapt_scale = adapt_scale
        
        self.suboptimal_r = None
        self.suboptimal_zca = None
        
        self.eq = eq




    def __eq__(self, other):
        """Two instances of this class are considered equal if their coordinate
        r is equal and underwent the same amount of optimization."""
        if isinstance(other, AdaptiveReactionCoord):
            same = (self.Dt == other.Dt and
                    self.adaptive_interval == other.adaptive_interval and
                    self.spatial_tol == other.spatial_tol and
                    self.RC_optimization_steps == other.RC_optimization_steps)
            try:
                return (same and (self.r == other.r).all())
            except TypeError:
                return (same and self.r == other.r)
        return False


    def __repr__(self):
        return f'{self.__class__}(Dt_0: {self.Dt}, spatial_tol={self.spatial_tol},\
 adaptive_interval={self.adaptive_interval})'


    def optimize(self,
                 degree: int = None,
                 new_feature: np.ndarray = None,
                 description: str = None,
                 optimize_r_degree: int = None,
                 adapt_degree: int = None):
        """Takes array new_feature to update reaction coordinate r to update
        r as r' = f(r, new_feature), where f is a 2D polynomial of the specified
        degree (default: 4 terms). The total squared deviation over the trajectory
        is calculated on the updated reaction coordinate, r'."""

        if not np.any(self.ri_is_boundary):
            err = f'States A and B must be defined by passing \
an array of indices marking A and B to {self.__class__}.initialize_RC()'
            logger.exception(err)
            raise ValueError(err)
        if self.RC_optimization_steps == 0:
            logger.info(f'Optimization initiated using parameters:\
 {self.__class__}: degree={degree}, optimize_r_degree={optimize_r_degree},\
 adapt_degree={adapt_degree}')
        updated_r = self.r
        # Optionally update
        if degree is not None:
            updated_r = self._optimize_ry(updated_r, degree,
                                          new_feature, description)
        # Optionally update r without including a new feature
        if optimize_r_degree is not None:
            updated_r = self._optimize_r(updated_r, optimize_r_degree) 
        # Optionally update r with special focus on suboptimal regions on r
        if adapt_degree is not None:
            updated_r = self._optimize_r_spatial(updated_r,
                                                 self.RC_optimization_steps,
                                                 self.adaptive_interval,
                                                 self.spatial_tol,
                                                 adapt_degree,
                                                 self.ri_is_boundary,
                                                 self.Dt)

        self.r = updated_r
        self.RC_optimization_steps += 1
        Dr_squared = self.evaluate_Dr2(self.r)
        self.Dr_squared.append(Dr_squared)



    def _optimize_r_spatial(self,
                            r,
                            k: int,
                            adaptive_interval: int,
                            spatial_tol: float,
                            adapt_degree: int,
                            ri_is_boundary: np.ndarray,
                            Dt: int):
        adapt_frq = self.adapt_frq
        if k%adapt_frq == 0 and k >= adaptive_interval:
            # Every n steps, find least optimal region 
            if k%adaptive_interval == 0:
                r0, zc1_max_dev = self.find_maxzc1(r.numpy(),
                                                   Dt,
                                                   self.traj_weights,
                                                   self.traj_indices,
                                                   self.eq)
                self.suboptimal_r = r0
                self.suboptimal_zca = zc1_max_dev
                logger.info('Detected most suboptimal region.\n\
Optimizing RC around %.2f every %d steps' % (self.suboptimal_r, adapt_frq))

            # Every `adapt_frq` steps, optimize the same region if deviation
            # of Zc1 from the mean is still larger than the cutoff
            if self.suboptimal_zca > spatial_tol:
                if self.adapt_scale is None:
                    scale = np.random.random()
                else:
                    scale = self.adapt_scale
                fenv = laplace_distribution(r, self.suboptimal_r, scale)
                updated_r = self._optimize_r(r, adapt_degree, fenv)
                return updated_r
        else:
            return r


    @staticmethod
    def find_maxzc1(r, Dt, traj_indices: List[int], traj_weights: List[float] = None,
                    eq: bool = True) -> Tuple[float, float]:
        """Find the region where the RC is most suboptimal"""
        x, zc1 = AdaptiveReactionCoord.compute_Z_ca(r, 1, traj_indices,
                                                    traj_weights, dx=0.001,
                                                    dt=Dt, eq=eq)
        mean_zc1 = np.mean(zc1)
        norm_zc1 = zc1/mean_zc1 - np.ones(len(zc1))
        x_max, zc1_max_dev = x[np.argmax(norm_zc1)], norm_zc1.max()
    
        return x_max, zc1_max_dev


    @staticmethod
    def optimize_temporal():
        raise NotImplementedError


    def make_boundaries_periodic(self):
        """Introduces helical boundary conditions to ensure the mean
        displacement from boundary states is still 0 even with time step
        Dt > Dt0."""
        raise NotImplementedError

        if not np.any(self.ri_is_boundary):
            err = f'States A and B must be defined by passing \
an array of indices marking A and B to {self.__class__}.initialize_RC()'
            logger.exception(err)
            raise ValueError(err)

        logger.info("Introduced helical boundary conditions to enable adaptive Dt.")
        self.periodic_boundaries = True



    def set_Dt(self, dt: int) -> None:
        raise NotImplementedError
        self.Dt = dt
        logger.info(f"Set new timestep: {dt}.")
