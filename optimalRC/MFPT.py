#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 22 14:01:41 2022

@author: jwidmer
"""
import logging
import math
import numpy as np
import tensorflow as tf
from typing import List, Tuple
from .constants import K_BOLTZMANN
from .BasisFunctions import basis_poly_r, basis_poly_ry
logger = logging.getLogger(__name__)


class MFPT:
    
    def __init__(self):
        """MFPT-like reaction coordinate as detailed in Eq. 27a-c
        in https://pubs.acs.org/doi/10.1021/acs.jctc.1c00218."""
        self.traj_length = None
        # Partitioning of trajectory
        self.state_A = None
        # Two frames from the same trajectory are marked with the same integer
        self.traj_indices = None
        # traj_breaks(i)=1 if X(i) and X(i+1) belong to the same trajectory
        self.traj_breaks = None

        # Progress of optimization & parameters
        self.Dt = 1
        self.r = None
        self.ri_is_boundary = None
        self.r_natural = None
        self.weights = None # weights to reweight r to equilibrium; reset when r is optimized
        self.weights_optimization_steps = 0 # optimization steps; reset when r is optimized

        self.RC_optimization_steps = 0
        self.mfpt_max = None
        self.selected_features_descr = None



    def __call__(self, **kwargs):
        """Calls MFPT.update_reaction_coord to optimize the reaction coordinate.
        kwargs to specify 'degree' of polynomial."""
        return self.optimize(**kwargs)


    def __repr__(self):
        return f"{self.__class__}()"


    def __str__(self):
        return f'{self.__class__}\n{self.RC_optimization_steps} optimization steps,\
 maximal MFPT = {self.mfpt_max[-1]:.3f} steps'


    def __eq__(self, other):
        """Two instances of this class are considered equal if their coordinate
        r is equal and underwent the same amount of optimization."""
        if isinstance(other, MFPT):
            try:
                return (self.r == other.r and
                        self.RC_optimization_steps == other.RC_optimization_steps)
            except ValueError:
                return ((self.r == other.r).all() and
                        self.RC_optimization_steps == other.RC_optimization_steps)
        return False


    def initialize_RC(self, state_A: np.ndarray,
                      traj_indices: np.ndarray = None) -> None:
        """Provide states A and B each as non-intersecting indices of frames
        in the whole trajectory.
        states_AB needs to be a 1D time series where A = 0, B = 1,
        and in (0, 1) elsewhere.
        Note: Can also supply a pre-optimized coordinate r to continue optimization."""
        self.r = state_A
        self.traj_length = self.r.shape[0]

        if traj_indices is None:
            traj_indices = np.ones(self.traj_length, np.float64)
        # Check that it's an array of right shape
        elif np.array(traj_indices).shape != (self.traj_length, ):
            err = f'"traj_indices" needs to be an array of shape {(self.traj_length, )}'
            logger.exception(err)
            raise ValueError(err)
            
        traj_breaks = np.ones_like(traj_indices, np.float64)
        traj_breaks[traj_indices != np.roll(traj_indices, -1)] = 0 
        
        self.traj_indices = np.array(traj_indices, np.float64)
        self.traj_breaks = traj_breaks

        # Frame indices where the trajectory is in state A or B, respectively
        self.state_A = np.where(state_A == 0.)[0]
        self.ri_is_boundary = np.zeros((1, self.traj_length))[0]
        self.ri_is_boundary[self.state_A] = 1

        logger.info(f'Defined states.\nA: {len(self.state_A)} instances.')
        out_of_A, into_A, total_transitions = self.count_transitions(state_A,
                                                                     traj_indices)
        self.out_of_A, self.into_A, self.total_transitions = (out_of_A, into_A,
                                                              total_transitions)
        logger.info(f'Observed transitions:\n\
{out_of_A} out of A,\n{into_A} into A, \n{total_transitions} total.')
        
        self.mfpt_max = list()
        mfpt_max = self.r.max()
        self.mfpt_max.append(mfpt_max)

        self.RC_optimization_steps = 0
        # Reset weights when r is changed
        self.weights = tf.ones_like(traj_indices, np.float64)
        logger.info('Initialized weights to 1.')
        self.weights_optimization_steps = 0
        logger.info(f'Assumed trajectory length: {len(state_A)} snapshots.')
        logger.info(f'Individual trajectories: {len(set(traj_indices))}.')



    @staticmethod
    def count_transitions(state_A: List[float], traj_indices: List[int] = None):
        """Count for every individual trajectory how often the boundary state
        is entered and left."""
        out_of_A = 0
        into_A = 0
        if traj_indices is None:
            traj_indices = np.ones(state_A.shape, np.float64)
        for itraj in np.unique(traj_indices):
            # Filter frames only in one consecutive trajectories
            active = state_A[np.where(traj_indices == itraj)]
            # Discretize: Boundary states (A) are 0, non-boundary 1.
            boundary_states = np.ceil(active)
            # counts of values [-1, 0, 1] in np.diff
            unique, counts = np.unique(np.diff(boundary_states), return_counts=True)
            # Make dict to enable default values via .get method
            counts = dict(zip(unique, counts))
            out_of_A += counts.get(1, 0) # How many diff = 1 (A=0 -> 1)
            into_A += counts.get(-1, 0) # How many diff = -1 (1 -> A=0)
        total_transitions = out_of_A + into_A
        return out_of_A, into_A, total_transitions


    def optimize(self,
                 degree: int = None,
                 new_feature: np.ndarray = None,
                 description: str = None,
                 optimize_r_degree: int = None) -> None:
        """Takes array new_feature to update reaction coordinate r to update
        r as r' = f(r, new_feature), where f is a 2D polynomial of the specified
        degree (default: 4 terms). The total squared deviation over the trajectory
        is calculated on the updated reaction coordinate, r'."""
        
        if not np.any(self.ri_is_boundary):
            err = 'States A and B must be defined by passing \
an array of indices marking A and B to ReactionCoord.initialize_RC()'
            logger.exception(err)
            raise ValueError(err)
        if self.RC_optimization_steps == 0:
            logger.info(f'Optimization initiated using parameters:\
 {self.__class__}: degree={degree}, optimize_r_degree={optimize_r_degree}')
        updated_r = self.r
        # Optionally update r with a new feature
        if degree is not None:
            updated_r = self._optimize_ry(updated_r, degree,
                                          new_feature, description)
        # Optionally update r without including a new feature
        if optimize_r_degree is not None:
            updated_r = self._optimize_r(updated_r, optimize_r_degree) 

        self.r = updated_r
        self.RC_optimization_steps += 1
        mfpt_max = self.r.numpy().max()
        self.mfpt_max.append(mfpt_max)
        # Remove weights when r is changed
        self.weights = tf.ones_like(self.r, np.float64)
        self.weights_optimization_steps = 0



    def _optimize_ry(self, updated_r: np.ndarray, degree: int,
                     new_feature: np.ndarray, description: np.ndarray,
                     fenv: np.ndarray = None) -> np.ndarray:
        # Check if feature has the right shape and add the description
        new_feature = self.add_feature(new_feature, description)
        # Calculate polynomial basis functions of r and y of given degree
        basis_functions = basis_poly_ry(self.r, new_feature,
                                        degree=degree, fenv=fenv)
        # Use optimal coefficients with given basis functions to find r
        # that minimizes Dr^2
        basis_fn_b, coeffs = self.calculate_optimal_coeffs(self.r,
                                                           basis_functions,
                                                           self.ri_is_boundary,
                                                           self.traj_breaks)
        updated_r = self.update_r(self.r, basis_fn_b, coeffs)
        return updated_r


    def _optimize_r(self, updated_r: np.ndarray,
                    degree: int = None, fenv: np.ndarray = None) -> np.ndarray:
        basis_functions = basis_poly_r(self.r, degree=degree, fenv=fenv)
        basis_fn_b, coeffs = self.calculate_optimal_coeffs(updated_r,
                                                           basis_functions,
                                                           self.ri_is_boundary,
                                                           self.traj_breaks)
        updated_r = self.update_r(updated_r, basis_fn_b, coeffs)
        return updated_r

    
    def optimize_weights(self, degree: int = 5):
        """Reweight snapshots to equilibrium by
        w(i)\sum_j n(j|i) = \sum_j w(j) n(i|j). Weight of i times all the out-
        going transitions from i is equal to the sum """
        # initialize weights
        basis_fn = basis_poly_ry(self.weights, self.r, degree)
        coeffs = self.calculate_weight_coeffs(self.weights, basis_fn,
                                              self.traj_breaks)
        updated_weight = self.weights + tf.tensordot(coeffs, basis_fn, 1)

        self.weights = updated_weight
        self.weights_optimization_steps += 1


    @staticmethod
    @tf.function
    def update_r(r: np.ndarray,
                 fk: tf.Tensor,
                 coeffs: np.ndarray) -> np.ndarray:
        """Add linear combination of basis functions with their coefficient to r"""
        rn = r + tf.tensordot(coeffs, fk, 1)
        # updated RC is clipped to be in [0, +\inf]
        rn = tf.clip_by_value(rn, 0, 1e10)
        return rn

    
    @staticmethod
    @tf.function
    def calculate_optimal_coeffs(r: tf.Tensor,
                                 fk: tf.Tensor,
                                 ri_is_boundary: np.ndarray,
                                 traj_breaks: np.ndarray) -> Tuple[tf.Tensor, tf.Tensor]:
        """ implements NPq (non-parametric MFPT optimization) iteration.

        r is the putative RC time-series
        fk are the basis functions of the variation delta r
        Ib is the boundary indicator function:
            Ib(i)=1 when X(i) belongs to the boundary states and 0 otherwise
        """
        # replacement for partitioning trajectory; selects only frames that
        # do not belong to boundary conditions and are free to be changed
        fk = fk * (1 - ri_is_boundary)
        # Calculate fk[i+1] - fk[i]
        delta_fk = fk - tf.roll(fk, shift=-1, axis=1)
        # Matrix A from https://pubs.acs.org/doi/pdf/10.1021/acs.jctc.1c00218,
        # equation (12b)
        Akj = tf.tensordot(fk*traj_breaks, delta_fk, axes=[1, 1])
        # Calculate r[i] - r[i+1] + \Delta t_0 in terms of time steps,
        # meaning \Delta t_0 = 1
        Dt0 = 1
        # Eq. (27c) from https://pubs.acs.org/doi/pdf/10.1021/acs.jctc.1c00218
        delta_r = tf.roll(r, shift=-1, axis=0) - r + Dt0
        # Calculate b from Ax = b
        b = tf.tensordot(fk, delta_r*traj_breaks, 1)
        b = tf.reshape(b, [b.shape[0], 1])
        # Compute minimum-norm least squares solution of Ax = b
        # to find optimal coeffs minimizing Dr^2;
        # condition number tends to be high despite A being symmetric in principle
        al_j = tf.linalg.lstsq(Akj, b, fast=False)
        al_j = tf.reshape(al_j, [al_j.shape[0]])
        return fk, al_j
    
    
    @staticmethod
    @tf.function
    def calculate_weight_coeffs(r: tf.Tensor, fk: tf.Tensor,
                                traj_breaks: np.ndarray) -> tf.Tensor:
        """Implements NPNEw (non-parametric non-equilbrium re-weighting factors 
        optimization) iteration.
        
        r is the putative RC time-series
        fk are the basis functions of the variation delta r
        traj_breaks is the trajectory indictor function:
            traj_breaks(i)=1 if X(i) and X(i+1) belong to the same short trajectory
            traj_breaks prevents contribution of frames not from same trajectory.
        """

        # \Delta fk = fk(t + dt) - fk(t)
        dfk = fk - tf.roll(fk, -1, 1)
        # bk = - \sum_t \Delta fk * r; trajbreaks kills cross terms
        b = - tf.tensordot(dfk, r*traj_breaks, 1)
        b = tf.reshape(b, [b.shape[0], 1])
        scale = tf.math.reduce_sum(1 - r * traj_breaks)
        scale = tf.reshape(scale, [1, 1])
        b = tf.concat((b, scale), 0)
        # scale to make sum to 1?

        ones = tf.reshape(traj_breaks, [1, traj_breaks.shape[0]])
        # \Delta fk = fk(t + dt) - fk(t); kill cross terms, append ones
        dfk = tf.concat((dfk * traj_breaks, ones), axis=0)
        # A_kj = \sum_t \Delta fk * fj * traj_breaks;
        akj = tf.tensordot(dfk, fk, axes=[1, 1])
        # Parameters: Solve A_kj \dot al_j = b
        al_j = tf.linalg.lstsq(akj, b, fast=False)
        al_j = tf.reshape(al_j, [al_j.shape[0]])

        return al_j


    @staticmethod
    def compute_Z_h(r: tf.Tensor, weights: List[float] = None,
                    dx: int = 0.1) -> Tuple[List[float], List[float]]:
        """ compute Zh, histogram-based partition function/probability
        r - list of coordinates, e.g., a trajectory time-series
        dx - bin size
        weights - list of weights, for re-weighting.
        """
        # Almost the same as np.histogram; instead of bin edges, the first
        # return argument are the floored values of r_i, the second argument
        # are the statistical weight; could replace at some point by
        # selecting bin_edges[0], bin_edges[-1], and then all the midpoints of the bin
        # edges in between from:
        # np.histogram(r, bins=np.arange(min(r), max(r) + binwidth, binwidth))
        ### NOT QUITE CORRECT!? Perhaps because here the value is floored,
        # while otherwise bins might be symmetric in np
        if weights is None:
            weights = list()

        zh = dict()
        if any(weights):
            for x, w in zip(r, weights):    
                x = math.floor(x/dx + 0.5) * dx
                zh[x] = zh.get(x, 0) + w
        else:
            for x in r:
                # x divided by binsize rounded up to next higher integer * dx
                x = math.floor(x/dx + 0.5) * dx
                # Add a count to the value in the dictionary
                zh[x] = zh.get(x, 0) + 1

        # convert dictionary to two lists, sorted by coordinate r
        r = list(zh.keys())
        r.sort()
        Z_h = [zh[x] / dx for x in r]

        return r, Z_h


    @staticmethod
    def compute_Z_ca(r: List[float], a: int, traj_indices: List[int] = None,
                     weights: List[float] = None, dt: int = 1, strict: bool = False,
                     dx: float = 2e-3, mindx: float = 1e-3,
                     eq: bool = True) -> Tuple[List[float], List[float]]:
        """Computes cut-based free energy profiles Z_C,a for non-equilbrium and equilbrium cases
        non-equilbrium case (eq=False) computes only outgoing part of the Z_C,a,
        which for a=1 gives Z_q - the non-equilbrium committor criterion
        equilbrium case (eq=True), computes outgoing and ingoing parts of Z_C,a
        can be used with a single trajectory and with ensamble of trajectories
        accepts re-weighting factors

        r - list of coordinates, a trajectory time-series
        a  - exponent of the distance: (|xi-xj|)^a
        itraj - trajectory index time-series:
            tells to which short trajectory point X(i) belongs to
            if itraj=[], a single long trajectory is assumed
        weights - list of weights or re-weighting factors
        dt - stepsize
        strict - whether to construct the exact profile,
            accurate representation of step functions
        dx - bin size for coarse-graining
        mindx - minimal value of dx:
            used to suppress errors for a<0
        eq - flag to compute equilibrium profile (eq=True), by including
            in-going transitions
        """
        if weights is None:
            weights = list() 
        if traj_indices is None:
            traj_indices = list()

        dzc = dict()
        tmax = len(r) - dt
        for i in range(tmax):
            # Skip iteration if r(i*dt) and r(i*dt+dt) are marked as belonging
            # to different trajectories
            if len(traj_indices) > 0 and traj_indices[i] != traj_indices[i+dt]:
                continue

            startx, x = (r[i], r[i+dt])

            if dx is not None and not strict: 
                # Rounds x to the nearest integer
                x = math.floor(x/dx + 0.5) * dx
                startx = math.floor(startx/dx + 0.5) * dx
            # Absolute displacement going from r(i*dt) to r(i*dt+dt)
            d = abs(x - startx)
            # assign d the value of mindx if d is smaller than d
            if a < 0:
                d = max(d, mindx)
            d = float(d)**a

            # Apply weight to d
            if any(weights):
                d = d * weights[i]    
            # Update coordinate value in Dict by absolute displacement d
            if startx < x:
                dzc[startx] = dzc.get(startx, 0) + d
                # Only update x if trajectory is not at equilibrium (Zq vs Zc1);
                # updating means including incoming trajectories into the sum
                if eq:
                    dzc[x] = dzc.get(x, 0) - d
            else:  
                dzc[startx] = dzc.get(startx, 0) - d
                if eq:
                    dzc[x] = dzc.get(x, 0) + d

        # Create lists sorted by coordinate value
        keys = list(dzc.keys())
        keys.sort()
        r_sorted = []
        Zc1 = []
        z = 0

        if eq:
            scale = 1/dt/2
        else:
            scale = 1/dt

        for x in keys:
            r_sorted.append(x)
            Zc1.append(float(z))
            z = z + dzc[x] * scale
            if strict:
                r_sorted.append(x)
                Zc1.append(float(z))
        # ly[0] equals 0, but a non-zero value is more convenient for further analysis
        if not strict and len(Zc1) > 1: 
            return r_sorted[1:], Zc1[1:] 
        return r_sorted, Zc1


    @staticmethod
    def compute_Z_q(**kwargs):
        """Non-equilibrium cut function"""
        # Set equilibrium to False, since explitictly Z_q is called
        kwargs['eq'] = False
        # Not sure if it can also be a != 1
        kwargs['a'] = 1
        return MFPT.compute_Z_ca(**kwargs)


    @staticmethod
    def compute_Zx_multi_Dt(q: np.ndarray, Dt_list: List[int], alpha: int,
                            **kwargs):
        """Calculate Zx profiles for multiple time steps provided by Dt_list."""
        dt_to_Zca = dict()
        for dt in Dt_list:
            x, zca = MFPT.compute_Z_ca(q, a=alpha, dt=dt, **kwargs)
            # Significant change only for very short trajectories compared to dt
            scale = (q.shape[0] - dt) / (q.shape[0]-1)
            zca = [zca_i / scale for zca_i in zca]
            dt_to_Zca[dt] = (x, zca)

        return dt_to_Zca


    def get_selected_features(self, idx: int = None) -> Tuple[str, np.ndarray]:
        """Returns the description of selected features used for optimizing the reaction coordinate
        based on index idx. idx can be a integer or a list of integers.
        Returns the last value if no index is specified."""
        if idx is None:
            idx = -1

        return self.selected_features_descr[idx]


    def add_feature(self, new_feature: np.ndarray = None,
                    description: str = None) -> np.ndarray:
        """Adds a new feature to the list of used features. If no new_feature
        is provided"""
        # Initialize list recording added features
        if self.selected_features_descr is None:
            self.selected_features_descr = list()

        # Check that it's an array of right shape
        elif new_feature.shape != (self.traj_length, ):
            err = f'Feature needs to be an array of shape {(self.traj_length, )}'
            logger.exception(err)
            raise ValueError(err)

        else:
            self.selected_features_descr.append(description)

        return new_feature


    @staticmethod
    def to_natural(q: np.array, dx: float, dtsim: float = 1.,
                   traj_indices: List[int] = None, weights: List[float] = None,
                   fixZhA: bool = True, zcm1: bool = False) -> np.ndarray:
        """ transforms putative commitor coordinate to the natural coordinate,
        where diffusion coefficient is constant. The following equations are used
        Zc1=dt*D*Zh (for small dt such that FEP~const)
        D=Zc1/(Zh*dt)
        dy/dx=D^-1/2

        q - committor time-series
        dx - bin size to compute the profiles and integrate
        dtsim - the trajectory/simulation sampling interval in time units in which D=1.
        itraj - trajectory index time-series:
            tells to which short trajectory point X(i) belongs to
            if itraj=None, a single long trajectory is assumed  
        weights - list of weights, for re-weighting,
            if lw=[], no re-weighting
        fixZhA - the leftmost value of Z_H, Z_H for state A, usually has a very large value,
            which leads to unphysical results. fixZhA=True assigns it to the next value 
        zcm1 - whether to use Z_C,-1 to estimate Z_H. Z_C,-1 gives a more robust estimate
            of Z_H if the sampling interval is sufficiently small.
        """
        if traj_indices is None:
            traj_indices = list()
        if weights is None:
            weights = list()

        if zcm1:
            _, lzcm1 = MFPT.compute_Z_ca(q, a=-1, traj_indices=traj_indices,
                                              dx=dx, dt=1, weights=weights, eq=True)
            lzh=[2*x for x in lzcm1]
        else:
            _, lzh = MFPT.compute_Z_h(q, weights=weights, dx=dx)
        lx1, lzc1 = MFPT.compute_Z_ca(q, a=1, traj_indices=traj_indices,
                                           dx=dx, dt=1, weights=weights, eq=True)
        # If boundary had imbalanced population
        if fixZhA:
            lzh[0] = lzh[1]

        x2y = dict()
        dydx = dict()

        xl, y, x2y[xl] = (0, 0, 0)

        for x, zh, zc1 in zip(lx1, lzh, lzc1):
            dydx[xl] = (zh * dtsim/zc1)**0.5 # = D^-1/2
            y = y + dydx[xl] * (x-xl)
            x2y[x] = y
            xl = x

        dydx[1.0] = 0
        x2y[1.0] = y
        q_natural = []
        for q_i in q:
            q0 = math.floor(q_i/dx + 0.5) * dx
            # Sometimes, q0 is rounded in a way that then does not allow
            # getting the value from the dict
            try:
                qn = x2y[q0] + (q_i - q0) * dydx[q0]
            except Exception as e:
                logger.info(e, q_i)
            q_natural.append(qn)

        return np.array(q_natural)


    @staticmethod
    def compute_FEP(r: List[float], T: int = 300, **kwargs)-> Tuple[np.ndarray]:
        """
        Computes Z_H aka probabilities in bins
        bin_size gives size of bins for calculation of Z_H
        r is the reaction coordinate; ideally the committor with D = 1
        T temperature in Kelvin."""
        KbT = K_BOLTZMANN * T
        # Z_H = Z_c,-1 * 2
        x, P = MFPT.compute_Z_ca(r, a=-1, **kwargs)
        FEP = -KbT * np.log(np.array(P) * 2) 
        return x, FEP
