#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 12:14:12 2021

@author: jwidmer
"""
import logging
import math
import numpy as np
from .util.util import rescale_r
from .RC import ReactionCoord
from scipy.integrate import simpson

logger = logging.getLogger(__name__)

class CommittorKinetics:

    def __init__(self, q, A: float, B: float, Dt: int,
                 resolution: float, use_transition_paths: bool, eq: bool,
                 traj_indices: list = None, weights: list = None, **kwargs):
        """Calculate kinetics of a system from the committor without fitting
        or assuming separation of timescales.
        For a reaction coordinate rescaled to constant unit diffusion q,
        calculate MFPT and MTPT for transitions between A and B as a function of q.
        The keyword resolution determines the resolution of the Z_c,a cut profiles.
        Dt_diffusive determines at which timestep q is diffusive. For this,
        Committor.compute_Zx_multi_Dt can be consulted to find the Dt, at
        which Z_c,1 remains constant in the transition region.
        
        use_transition_paths, bool
            mitigate boundary effects in calculation of Z_c1. These occur
            since at the boundaries, the mean squared displacement is non-zero.
            Depending on the choice of boundary, this can have an impact
            on the estimated kinetics. See https://doi.org/10.1021/ct3008292"""
        self.q = q
        self.Dt = Dt
        self.resolution = resolution
        self.use_transition_paths = use_transition_paths
        self.traj_indices = traj_indices
        self.weights = weights
        if traj_indices is None:
            self.traj_indices = np.ones(q.shape, np.float64)
        self.A = A
        self.B = B
        self.eq = eq
        # Length of trajectory
        self.n = len(q)
        ## Cut profiles
        # Zc1Committor
        if self.use_transition_paths == True:
 #            logger.warning('CARFEUL: This setting should probably not be used.\
 # It may lead to substantially overestimated MTPTs')
            ekn = self.transition_segment_summation(q)
            lx, lzc1 = self._comp_Zca_ekn(ekn, a=1, dx=resolution, eq=eq)
            self._lx, self.lzh = self._comp_Zca_ekn(ekn, a=-1, dx=resolution,
                                                    eq=eq)
            # ZH

        else:
            lx, lzc1 = ReactionCoord.compute_Z_ca(q, a=1, dx=resolution,
                                                  dt=Dt, eq=eq,
                                                  traj_indices=traj_indices,
                                                  **kwargs)
            self._lx, self.lzh = ReactionCoord.compute_Z_ca(q, a=-1,
                                                            dx=resolution, eq=eq,
                                                            dt=Dt,
                                                            weights=weights,
                                                            traj_indices=traj_indices,
                                                            **kwargs)
        self.zc1 = np.array(lzc1)
        self.lx = np.array(lx)
        self.zc1_inverse = 1/np.array(lzc1)
        # Transition region indices
        self.is_transition_region = np.zeros(len(lx))
        self.is_transition_region[(A < self.lx) & (self.lx < B)] = 1.
        # Rescale 
        self.rescaled_q = rescale_r(q, A, B)


    def __repr__(self):
        return f"""{self.__class__}(q_diffusive={self.q_diffusive}, A={self.A},
B={self.B}, Dt_diffusive={self.Dt_diffusive}, resolution={self.resolution},
use_transition_paths={self.use_transition_paths}, eq={self.eq})"""


    def estimate_flux(self, Dt_0):
        """Calculates equilibrium flux from the equilibrium cut profile Z_c1
        For the committor, where $Z_{C,1}(q)=const$, $N_{AB}=Z_{C,1}/[q(B)-q(A)]$.
    
        The equilibrium flux can be estimated as $J_{AB}=N_{AB}/T$,
        where $T=N\Delta t$, is the total length of trajectory and $N_{AB}$
        is the number of transitions from A to B. $N_{AB}$ can be computed as
        1/N_AB = \int_{q(A)}^{q(B)} 1/Z_C,1(q)dq = <1/Z_C,1>[q(B)-q(A)]
        Then, J = N_AB / (n_frames_total * DT_0)

        Integrate 1/Zc1 over transition region to get 1/J_AB = N_AB.

        Returns total number of transition in trajectory and the flux in time
        units of Dt_0. If, e.g., the sampling step of the input MD simulation
        is 200 ps, passing Dt_0 = 0.2 will give a result in ns^-1"""
        I = simpson(self.is_transition_region * self.zc1_inverse, self.lx)
        # Number of transitions between regions A and B in the simulated timeframe
        Nab_total = 1/I
        Jab = Nab_total / (self.n * Dt_0) # Get units of [Dt_0]^-1

        return Nab_total, Jab



    def calc_mean_fpt_AB(self, Jab: float) -> float:
        """Mean first passage time from  A to B as a function of the committor.
        For first order reaction kinetics, MFPT = 1/k."""
        mfpt_AB = np.average(1 - self.rescaled_q, weights=self.weights) / Jab
        return mfpt_AB


    def calc_mean_fpt_BA(self, Jab: float) -> float:
        """Mean first passage time from B to A as a function of the committor.
        For first order reaction kinetics, MFPT = 1/k."""
        mfpt_BA = np.average(self.rescaled_q, weights=self.weights) / Jab
        return mfpt_BA


    def calc_mean_tpt(self, Jab: float) -> float:
        """Mean transition path time as a function of the committor."""
        mtpt = np.average(self.rescaled_q*(1 - self.rescaled_q),
                          weights=self.weights) / Jab
        return mtpt


    def transition_segment_summation(self, xtraj):
        """ transition path segment counting scheme
        computes an equilibrium kinetic network - the number of transition between
        different points. It should be followed by function comp_Zca_ekn,
        which computes the cut-profiles.
        
        xtraj - trajectory time-series
        x0 - the position of the boundary of the left boundary state
        x1 - the position of the boundary of the right boundary state
        itraj - trajectory index time-series:
            tells to which short trajectory point X(i) belongs to
            if itraj=[], a single long trajectory is assumed
        dx - bin size for coarse-graining
        dt - the value of \Delta t
        """
        dt = self.Dt_diffusive
        dx = self.resolution
        itraj = self.traj_indices
        ekn = dict()
        # Map boundary points to their bin according to the resolution
        x0 = math.floor(self.A/dx+0.5) * dx
        x1 = math.floor(self.B/dx+0.5) * dx
    
        lx = list()
        if len(itraj) > 0: # iterate over an ensemble of trajectories
            current_traj = itraj[0]
            for x, it in zip(xtraj, itraj):
                if current_traj == it:
                    lx.append(x)
                else: 
                    ekn = self._extract_TPs_to_EKN(lx, x0, x1, dx, dt, ekn)
                    current_traj = it
                    lx = [x]
            ekn = self._extract_TPs_to_EKN(lx, x0, x1, dx, dt, ekn)
        else:
            ekn = self._extract_TPs_to_EKN(xtraj, x0, x1, dx, dt, ekn)
            
        for ij in ekn:
            # Calculate number of transitions from i to j per timestep
            ekn[ij] = ekn[ij] / dt   
        return ekn
    
    
    @staticmethod
    def _extract_TPs_to_EKN(traj, x0: float, x1: float, dx: float, dt: int,
                            ekn: dict) -> dict:
        """For every point in the 1D trajectory ('traj'), collect coordinates
        not part of boundaries, i.e. the transition path segments, in a list.
        Then, update the dictionary containing the number of transitions between
        nodes on a transition path segment using _update_EKN_with_TP()."""
        # List containing transition path
        lx = list()
        for x in traj:
            # Map coordinate according to resolution dx
            x = math.floor(x/dx+0.5) * dx
            if x <= x0: # Check if part of boundary A
                lx.append(x0)
                ekn = CommittorKinetics._update_EKN_with_TP(lx, x0, x1, dt, ekn)
                # Create new transition path segment since boundary is reached
                lx = [x0]
            elif x >= x1:  # Check if part of boundary B
                lx.append(x1)
                ekn = CommittorKinetics._update_EKN_with_TP(lx, x0, x1, dt, ekn)
                # Create new transition path segment since boundary is reached
                lx = [x1] 
            else: # Else append transition point
                lx.append(x)
        # update ekn with last path
        ekn = CommittorKinetics._update_EKN_with_TP(lx, x0, x1, dt, ekn)
        return ekn

    
    @staticmethod
    def _update_EKN_with_TP(traj, x0: float, x1: float, dt: int, ekn: dict) -> dict: # process a transition path (TP) segment
        """Update a dictionary containing the number of transitions between
        nodes on a transition path segment with transitions from a new
        transition path ('traj') and return the updated dict."""
        n = len(traj) 
        if n < 2: # Only process TP segments
            return ekn
        firstb = traj[0] <= x0 or traj[0] >= x1 # Check if TP starts at boundary
        lastb = traj[-1] <= x0 or traj[-1] >= x1 # Check if TP ends at boundary
        for i in range(1, n): # from i-dt to i
            j = i - dt
            if j < 0:
                if not firstb: # Skip iteration if TP does not start at B ???
                    continue
                j = 0
            key = traj[j], traj[i]
            ekn[key] = ekn.get(key, 0) + 1 # Add transition between nodes to EKN
        if lastb: # If TP ends in boundary, add transition to boundary
            for i in range(max(n-dt, 1), n-1):
                key = traj[i], traj[-1]
                ekn[key] = ekn.get(key, 0) + 1
        if firstb and lastb and dt > n-1: # If TP starts AND ends at boundary,
            key = traj[0], traj[-1] # Add another transition to ensure zero
            ekn[key] = ekn.get(key, 0) + dt - n+1 # correlation of Zc1 at boundary
        return ekn


    @staticmethod
    def _comp_Zca_ekn(ekn, a: int, dx: float, eq: bool, strict=False,
                      mindx: float = 1e-3):
        """computes cut-based free energy profiles Z_C,\alpha using the transition
        pathway segment summation scheme, for non-equilbrium and equilbrium cases
        used together with comp_ekn_tp function
        
        ekn - an equilibrium kinetic network computed by comp_ekn_tp
        a  - the value of alpha
        dx - bin size for coarse-graining
        strict - whether to construct the exact profile, accurate representation of step functions
        eq - flag to compute equilbrium profile, by including in-going transitions
        """
        import math
        dzc = {}
        for y, x in ekn:
            d = abs(y-x)
            if a < 0:
                d = min(d, mindx)
            else:
                d = abs(y-x)**a * ekn[(y,x)]
            if dx != None: 
                x=math.floor(x/dx+0.5)*dx
                y=math.floor(y/dx+0.5)*dx
            if y<x:
                dzc[y] = dzc.get(y,0) + d
                if eq:
                    dzc[x] = dzc.get(x,0) - d
            else:  
                dzc[y] = dzc.get(y,0) - d
                if eq:dzc[x] = dzc.get(x,0)+d
                
        keys = list(dzc.keys())
        keys.sort()
        lx = list()
        ly = list()
        z = 0
        scale = 1
        if eq:
            scale = 0.5
        for x in keys:
            lx.append(x)
            ly.append(float(z))
            z = z + dzc[x] * scale
            if strict:
                lx.append(x)
                ly.append(float(z))
        if not strict and len(ly)>1: # ly[0] equals 0, but a non-zero value is convenieint for further analysis
            return lx[1:],ly[1:] 
        return lx,ly 

