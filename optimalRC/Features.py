#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 16 21:08:53 2021

@author: julian
"""
import gc
import numpy as np
import pandas as pd
import mdtraj as md
import logging
from os.path import exists
from typing import List, Tuple, Dict
from sklearn.metrics import mutual_info_score
logger = logging.getLogger(__name__)

class FeatureFeeder:
    """
    Use call method to return a feature and its description (useful in a loop).
    Features can be read from an ASCII file (`from_ascii`) or generated on the
    fly.
    
    The number of features generated at once is controlled by `n_features`.
    If `reuse_features` is turned off, subsequent calls of the class instance
    exceeding `n_features`, new features are generated. Otherwise, the same
    features are cycled through in the same order.
    
    `filter_string` can be used to control which atoms are possible to select 
    (e.g. 'protein' or 'not resn water' can be used to ignore solvent molecules)."""

    def __init__(self, topology_file: str = None, trajectory_file: str = None,
                 stream: bool = None, n_features: int = None, atom_sel: str = 'all',
                 reuse_features: bool = False, shuffle: bool = False,
                 traj_length: int = None, **kwargs):
        self.md_load = kwargs
        if trajectory_file is not None:
            if not exists(trajectory_file):
                raise FileNotFoundError(f"{trajectory_file} not found.")
        self.trajectory_file = trajectory_file
        self.topology_file = topology_file
        self.stream = stream
        self.n_atoms = 2 # Only distances implemented for now; defined by n=2 atoms
        self.atom_sel = atom_sel
        self.traj_length = traj_length
        # Features and description
        self.generated_features = None
        self.generated_features_desc = None
        # Numbers of features generated (loaded in memory)
        self.n_features = n_features
        # How to handle regeneration of features
        self.reuse_features = reuse_features
        self.shuffle = shuffle
        self._count = 0
        if topology_file is not None:
            if not exists(topology_file):
                raise FileNotFoundError(f"{topology_file} not found.")
            self.topology = md.load(self.topology_file).topology
            logger.info(f'Loaded topology: {self.topology}')

            selected = self.topology.select(self.atom_sel)
            subset = self.topology.subset(selected)
            if atom_sel != 'all':
                logger.info(f'Used "{atom_sel}" to subset topology:\
{subset}')
            self.used_residue_names = [str(i) for i in subset.residues]

            logger.info(f"Used residue names: {len(self.used_residue_names)}")
            logger.info(f" {', '.join(self.used_residue_names)}")
           


    def __repr__(self):
        return f"{self.__class__}(topology_file={self.topology_file}, \
trajectory_file={self.trajectory_file}, stream={self.stream}, \
n_features={self.n_features}, atom_sel={self.atom_sel}, \
reuse_features={self.reuse_features}, kwargs={self.md_load})"
    
    
    
    def __eq__(self, other):
        if isinstance(other, FeatureFeeder):
            try:
                arrays = ((self.generated_features == other.generated_features).all() and
                          (self.generated_features_desc ==
                           other.generated_features_desc))
            except AttributeError:
                arrays = (self.generated_features == other.generated_features and
                          self.generated_features_desc == other.generated_features_desc)
            flags = (self.topology_file == other.topology_file and
                     self.trajectory_file == other.trajectory_file and
                     self.stream == other.stream and
                     self.atom_sel == other.atom_sel and
                     self.n_features == other.n_features and
                     self.reuse_features == other.reuse_features and
                     self._count == other._count)
            return arrays and flags
        return False



    def __call__(self, seed: int = None):
        # If there are no features yet or already cycled once through all features
        # and not recycling, generated fresh features
        if self.generated_features is None or (self._count == self.n_features
                                               and self.reuse_features == False):

            atom_inds = self.gen_random_atoms(self.n_atoms, self.topology,
                                              self.n_features, self.atom_sel,
                                              seed)
            _ = self.gen_features(atom_inds)
            self.reset_count()

        # Else, just set counter to 0 to recycle features
        elif self._count >= self.n_features:
            self.reset_count()
            if self.shuffle:
                n = self.generated_features.shape[1]
                shuffled_idx = np.random.choice(n, n, replace=False)
                self.generated_features = self.generated_features[:, shuffled_idx]
                self.generated_features_desc[:] = [self.generated_features_desc[i] for i in shuffled_idx]
                gc.collect()
            logger.info(f"Recycled features. Shuffled: {self.shuffle}")


        old_count = self._count
        self._count += 1
        return (self.generated_features[:, old_count],
                self.generated_features_desc[old_count])


    def __len__(self):
        try:
            return self.n_features
        except TypeError:
            return 0


    def __setitem__(self, feature_index, feature):
        try:
            self.generated_features[feature_index] = feature
        except TypeError:
            print('No features loaded yet; cannot assign feature to index.')



    def __getitem__(self, feature_index):
        try:
            return self.generated_features[feature_index]
        except TypeError:
            return


    def _get_trajectory(self, stream: bool = None) -> None:
        # File extensions are used by mdtraj automatically to call
        # format-specific loading functions

        if stream == False:
            t = md.load(self.trajectory_file,
                        top=self.topology_file,
                        **self.md_load)

        elif stream == True and type(self.trajectory_file) == str:
            t = md.iterload(self.trajectory_file,
                            top=self.topology_file,
                            **self.md_load)

        else:
            err = 'If "stream" is True, the trajectory has to be a single file \
supplied as a string.'
            logger.exception(err)
            raise ValueError(err)
        return t


    def _get_traj_length(self):
        if self.stream:
            self.traj_length = sum(x.n_frames for x in self._get_trajectory(self.stream))
        elif self.stream == False:
            self.traj_length = sum(1 for _ in self._get_trajectory(self.stream))
        logger.info(f'Generating features from: {self.__repr__()}')
        logger.info(f'Trajectory length: {self.traj_length}.')


    @staticmethod
    def gen_random_atoms(n_atoms: int, topology, n_features: int,
                         atom_sel: str = None, seed: int = None) -> np.ndarray:
        """Generate n_feat"""
        active_atoms = topology.select(atom_sel)
        # randomly choose two protein atoms without replacement;
        # same feature can potentially be selected twice
        np.random.seed(seed)
        atom_indices = np.c_[[np.random.choice(active_atoms, n_atoms, replace=False)
                              for _ in range(n_features)]]
        return atom_indices


    def gen_features(self, atom_indices: np.ndarray = None) -> Tuple[np.ndarray,
                                                                     List[str]]:
        
        if atom_indices is None:
            atom_indices = self.gen_random_atoms(self.n_atoms, self.topology,
                                                 self.n_features, self.atom_sel)
        n_distances = self.n_features

        if self.traj_length is None:
            self._get_traj_length()
        self.generated_features = None
        features = np.zeros(shape=(self.traj_length, self.n_features))
        gc.collect()

        f = md.compute_distances
        logger.info(f'Generating {n_distances} new features using {f.__name__}...')

        if self.stream == True:
            n_prev = 0
            for chunk in self._get_trajectory(self.stream):
                n_frames = chunk.n_frames
                features[n_prev:n_prev + n_frames,
                         :] = f(chunk, atom_indices).reshape(n_frames, n_distances)
                n_prev += n_frames
        elif self.stream == False:
            features = f(self._get_trajectory(self.stream), atom_indices)

        description_strings = self.generate_description(atom_indices, self.topology)
        self.generated_features_desc = description_strings
        self.generated_features = features
        return self.generated_features, self.generated_features_desc


    def reset_count(self):
        """Resets the counter on which features are supplied upon call;
        helpful for reproducibility."""
        self._count = 0



    @staticmethod
    def generate_description(atom_indices, topology: None) -> List[str]:
        """Generate a description for the atom indices provided. Used as
        header when saving features as csv and returned in __call__."""
        if topology is None:
            # Generate generic description in case no topology is provided
            [f'Feature {i}' for i in range(atom_indices.shape[0])]
        else:
            # Use atom names in case a topology was supplied
            description_strings = [', '.join((str(topology.atom(i[0])),
                                              str(topology.atom(i[1]))))
                                   for i in atom_indices]
            return description_strings


    @staticmethod
    def calc_MIs(features: np.ndarray, r: np.ndarray,
                 feature_desc: List[str], bins: int,
                 MIs: dict = None) -> Dict[str, float]:
        """Calculate mutual information MI for feature columns and the reaction
        coordinate to extract features which are informative on the FEP on r.
        MI = H(x) + H(y) - H(x, y)
        
        features
            array of shape (n_frames, n_features)
        r
            array of shape (n_frames,), the reaction coordinate.
        feature_desc
            list of strings of length n_features, contains info on the
            features.
        bins
            int or tuple of int is passed to np.histogram and determines
            the resolution.
        MIs, optional
            Dictionary potentially containing already some MIs, such that it
            can be updated with new features."""
        
        if MIs is None:
            MIs = dict()
        for i in range(features.shape[1]):
            x = features.T[i] # extract ith column
            desc = feature_desc[i]
            if desc in MIs:
                continue
            # Must set density to FALSE to prevent math domain error in MI
            p_xy = np.histogram2d(x, r, bins, density=False)[0]
            mi = mutual_info_score(None, None, contingency=p_xy)
            MIs[desc] = mi
        # Return the dictionary sorted by the MI: Highest MI comes first.
        return dict(sorted(MIs.items(), key=lambda x: x[1], reverse=True))


    def to_ascii(self, fname: str, data_format: str = 'csv', **kwargs) -> None:
        """mode, str: {'csv', 'hdf'}
                Controls in which data format to write data."""
        if self.generated_features is None:
            logger.error('No features available; use `gen_random_feature`\
or `from_ascii` to load features.')
        elif data_format == 'csv':
            X = pd.DataFrame(self.generated_features,
                             columns=self.generated_features_desc)
            X.to_csv(fname, index=False, **kwargs)
        elif data_format == 'hdf':
            X = pd.DataFrame(self.generated_features,
                             columns=self.generated_features_desc)
            X.to_hdf(fname, **kwargs)



    def from_ascii(self, fname: str, keep_header: bool = True,
                   data_format: str = 'csv', **kwargs):
        """ Array of features with shape=(n_features, n_frames).
        header, Boolean=False:
                The header of the csv file is overwritten by
                enumerating features.
        data_format, str: {'csv', 'hdf'}:
                Controls in which data format to write data.
        Courtesy of A.V.: bash tools are probably fastest to load individual
        columns (albeit sequential)."""
        if data_format == 'csv':
            X = pd.read_csv(fname, **kwargs)
        elif data_format == 'hdf':
            X = pd.read_hdf(fname, **kwargs)

        self.generated_features = X.values
        self.n_features = X.shape[1]
        self.traj_length = X.shape[0]
        logger.info(f'Read {self.n_features} features from {fname}...')
        logger.info(f'Trajectory length: {self.traj_length}.')
        self.reuse_features = True
        logger.info('Turned on reusing features.')
        if keep_header == True:
            self.generated_features_desc = X.columns
        else:
            self.generated_features_desc = [f'Feature {i}' for i in range(X.shape[1])]



