    #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 22:17:19 2021

@author: julian
"""

import os
import logging.config
import yaml
from pathlib import Path


PROJECT_ROOT_PATH = Path(__file__).parent.parent.parent.absolute()
LOGGING_CFG_PATH = PROJECT_ROOT_PATH / 'logging_config.yaml'
LOGGER = logging.getLogger(__name__)


def setup_logging(LOGS_DIR, cfg_path: Path = LOGGING_CFG_PATH,
                  default_level: int = logging.INFO) -> None:
    """Load logging config, setup log output dir."""
    # Create logging directory if not present
    os.makedirs(LOGS_DIR, exist_ok=True)
    # Create logging files if not present
    if not 'errors.log' in os.listdir(LOGS_DIR):
        (LOGS_DIR / 'errors.log').touch()
    if not 'info.log' in os.listdir(LOGS_DIR):
        (LOGS_DIR / 'info.log').touch()

    if cfg_path.exists():
        with cfg_path.open() as cfg_obj:
            config = yaml.safe_load(cfg_obj.read())

        # make sure the log directory is always at the project root and not relative to the config call location
        for handler_name, handler_dict in config['handlers'].items():
            if 'filename' in handler_dict.keys():
                config['handlers'][handler_name]['filename'] = os.path.join(LOGS_DIR, handler_dict['filename'])
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)



def clean_logs(LOGS_DIR, log_dir_path: str) -> None:
    """Erases all content in the log files. Deletes the contents, but not files."""
    # Empty contents of current logfiles
    for file_path in os.listdir(log_dir_path):
        if os.path.exists(log_dir_path / file_path):
            with open(log_dir_path / file_path, "w"):
                pass  # will empty the file
    LOGGER.info(f'Cleaned log dir {LOGS_DIR}')
