#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 26 17:48:32 2021

@author: jwidmer
"""
import pickle
import logging
import numpy as np
import pandas as pd

from typing import List
logger = logging.getLogger(__name__)

def rescale_r(r: np.ndarray, lower: float, upper: float) -> np.ndarray:
    """Rescale reaction coordinate to linearly interpolate
    between states A and B."""
    if not lower < upper:
        err = f'lower ({lower}) must be less than upper ({upper}).'
        raise ValueError(err)
    r_scaled = r.copy()
    intermediate = np.where((r > lower) & (r < upper))[0]
    r_scaled[r <= lower] = 0.
    r_scaled[r >= upper] = 1.
    r_scaled[intermediate] = (r[intermediate] - lower) / (upper - lower)
    return r_scaled
    

def save_model(filename, obj):
    with open(filename, 'wb') as file:
        pickle.dump(obj, file)
    logger.info(f'Saved {obj} to {filename}.')



def load_model(filename):
    with open(filename, 'rb') as file:
        other = pickle.load(file)

    logger.info(f'Loaded {other} from {filename}.')
    return other


def map_ts_on_FEP(r, binned_r, timeseries, summary: str = 'mean', **kwarg):
    """If the original RC r and bins along the RC (for example output from
    compute_Zca) are provided along with a time series, the time series
    is mapped to the binned RC by calculating the average of the time series
    within the bin. In case the time series is a matrix, its shape should be
    n_frames x n_features, and **kwarg can be used
    to supply an axis along which the summary (e.g., mean) is computed."""
    ts_to_r = dict()
    r_prev = 0.
    for ri in binned_r:
        members = np.where((r >= r_prev) & (r <= ri))[0]
        if summary == 'mean':
            ts_to_r[ri] = timeseries[members].mean(**kwarg)
        elif summary == 'std':
            ts_to_r[ri] = timeseries[members].std(**kwarg)
        elif summary == 'iqr':
            q75, q25 = np.percentile(timeseries[members], [75, 25], **kwarg)
            iqr = q75 - q25
            ts_to_r[ri] = iqr
        elif summary == 'cv':
            ts_to_r[ri] = timeseries[members].std(**kwarg)/timeseries[members].mean(**kwarg)
        elif summary == 'bin_imbalance':
            one = timeseries[members].sum(**kwarg)
            zero = timeseries[members].shape[0] - one 
            ts_to_r[ri] = 1 - (abs(one - zero) / (zero + one))
        r_prev = ri
    mapped_ts = list(ts_to_r.values())
    return [mapped_ts]


def parse_tracefile(tracefile: str, breaklinks: str = 'all'):
    """Read the log containing a BRKLNKREPORT from Campari and parse it to
    a data frame."""
    with open(tracefile, 'r') as fl:
        log = fl.read()

    if breaklinks == 'all':
        report_end = '----------- END OF REPORT'
    elif breaklinks == 'breaks':
        report_end = ' In addition, there are '
    # In addition, there are  ## Use this for only breaks
    linktable = log.split(report_end)[0].split('INDEX "T"|')[1]
    trajlinks = pd.DataFrame([line.split() for line in linktable.split('\n')[1:-1]],
                             columns=['from', 'to', 'from_index', 'to_index'], dtype=float)

    return trajlinks


def traj_ids_from_PIGStrace(df, outname: str = 'trajid', save: bool = False):
    """From a df containing ALL trajectory links from a PIGS run,
    generate a df containing the traj_id."""
    
    replica_boundaries = [0] + list(np.where(df['from'].diff() == 2)[0])
    for i, idx in enumerate(replica_boundaries):
        df.loc[idx+i-0.5] = [np.nan, np.nan, np.nan, np.nan]
        df = df.sort_index().reset_index(drop=True)

    # Subtract successive frames; for reseedings, the diff is not 1
    df['diff'] = df.iloc[:, 1] - df.iloc[:, 0]
    start = 0
    traj_nr = 0
    # Every frame that is reseeded or on trajectory boundary
    reseeding_ids = df[(df['diff'] != 1)].index
    # assign the same integer to the frames between reseeding events
    for jump_idx in reseeding_ids[1:]:
        df.loc[start:jump_idx-1, 'traj_id'] = traj_nr
        start = jump_idx
        traj_nr += 1
    
    # The last trajectory that is not reseeded anymore
    df['traj_id'].fillna(traj_nr, inplace=True)

    if save:
        df.to_csv(f'{outname}.csv')
    return df
