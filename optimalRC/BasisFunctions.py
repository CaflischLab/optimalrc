#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 12:03:26 2021

@author: jwidmer
"""
import numpy as np
import tensorflow as tf

@tf.function
def basis_poly_ry(r: np.ndarray, y: np.ndarray, degree: int = 4, fenv=None) -> tf.Tensor:
    """computes basis functions as terms of polynomial of variables r and y

    r is the putative RC time-series
    y is a randomly chosen collective variable or coordinate to improve r
    n is the degree of the polynomial
    fenv is common envelope to focus optimization on a particular region
    """
    # Normalize features
    r = r / tf.math.reduce_max(tf.math.abs(r))
    y = y / tf.math.reduce_max(tf.math.abs(y))

    if fenv is None:
        f = tf.ones_like(r)
    else:
        f = tf.identity(fenv)

    fk = []
    for r_i in range(degree+1):
        fy = tf.identity(f)
        for _ in range(degree+1 - r_i):
            fk.append(fy)
            fy = fy*y
        f = f*r
    return tf.stack(fk)


@tf.function
def basis_poly_r(r: np.ndarray, degree: int = 12, fenv=None) -> tf.Tensor:
    """Polynomial function acting on r only, without new feature.
    r is the putative RC time-series
    Ib is the boundary indicator function:
    Ib(i)=1 when X(i) belongs to the boundary states and 0 otherwise
    n is the degree of the polynomial
    fenv is common envelope to focus optimization on a particular region"""
    r=r/tf.math.reduce_max(tf.math.abs(r))

    if fenv is None:
        f=tf.ones_like(r)
    else:
        f=tf.identity(fenv)

    fk=[]
    for _ in range(degree+1):
        fk.append(f)
        f=f*r

    return tf.stack(fk)


def laplace_distribution(r, r0, scale: float) -> tf.Tensor:
    """Defines neighbourhood around the maximal Z_c1 that is affected
    by optimization of Dr^2 using a polynomial of higher degree."""
    d = 0.1 ** (1 + 3 * scale)
    fenv = tf.math.exp(-tf.math.abs(r0 - r) / d)
    return fenv


def sigmoid(x: np.ndarray, x0: float, scale: float) -> np.ndarray:
    return 1 - (1 / (1 + np.exp( -(x - x0)/scale)) )


def parabola(x: np.ndarray, a: float, c: float) -> np.ndarray:
    """Returns parameters of the form f = 1/2 kx^2 + c = ax^2 + c
    No first order term to have harmonic potential centered around local
    extremum."""
    return a*np.array(x)**2 + c