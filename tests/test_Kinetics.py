#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 18 12:27:14 2022

@author: jwidmer
"""

import unittest
import numpy as np

from optimalRC.Kinetics import CommittorKinetics

class TestKinetics(unittest.TestCase):
    
    
    dx = 0.01
    def estimate_flux(self):
        dx = self.dx
        r = np.array([0, 0, 0.25, 0.5, 1])


        kin = CommittorKinetics(r, A=0., B=1., Dt_diffusive=1, resolution=dx,
                                use_transition_paths=False, eq=False, strict=False)
        assert np.allclose(kin.lx, kin.zc1)
        assert np.allclose(kin.zc1_inverse, [4., 2.])
        print(kin.estimate_flux(Dt_0=1))

    def calculate_MFPTs(self):
        dx = self.dx
        r = np.array([0, 0, 0.5, 0.5, 1])
        flux = 1
        factor = 2
        kin = CommittorKinetics(r, A=0., B=1., Dt_diffusive=1, resolution=dx,
                                use_transition_paths=False, eq=False, strict=False)
        mfpt_ab = kin.calc_mean_fpt_AB(Jab=flux)
        assert mfpt_ab == 0.6
        assert kin.calc_mean_fpt_AB(Jab=factor*flux) == mfpt_ab / factor
        kin2 = CommittorKinetics(1-r, A=0., B=1., Dt_diffusive=1, resolution=0.1,
                                use_transition_paths=False, eq=False, strict=False)
        assert kin.calc_mean_fpt_BA(flux) == kin2.calc_mean_fpt_AB(flux)


    def calculate_MTPT(self):
        dx = self.dx
        r = np.array([0, 0, 0.5, 0.5, 1])
        flux = 1
        factor = 2
        kin = CommittorKinetics(r, A=0., B=1., Dt_diffusive=1, resolution=dx,
                                use_transition_paths=False, eq=False, strict=False)
        mtpt = kin.calc_mean_tpt(Jab=flux)

        assert mtpt == 0.1
        assert kin.calc_mean_tpt(Jab=factor*flux) == mtpt / factor
