#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 16 21:08:21 2021

@author: julian
"""
import os
import unittest
from optimalRC.Features import FeatureFeeder

class Test_FeatureFeeder(unittest.TestCase):
    path = os.path.dirname(__file__)
    fname = f'{path}/test_data/features.npy'
    top = f'{path}/test_data/top_test.pdb'
    traj = f'{path}/test_data/traj_test.xtc'


    def test_generate_random_features(self):
        f = FeatureFeeder(trajectory_file=self.traj,
                          topology_file=self.top,
                          stream=True,
                          n_features=2,
                          reuse_features=True,
                          **dict(stride=1, atom_indices=None, chunk=10000))

        feat, desc = f()
        for i in range(3):
            _ = f()
        return f


    def test_trajlength(self):
        f = FeatureFeeder(trajectory_file=self.traj,
                          topology_file=self.top,
                          stream=True,
                          n_features=2,
                          reuse_features=True,
                          **dict(stride=1, atom_indices=None, chunk=10000))
        f._get_traj_length()
        assert f.traj_length == 6


    def test_generate_random_features_noStream(self):
        f = FeatureFeeder(trajectory_file=self.traj,
                          topology_file=self.top,
                          stream=False,
                          n_features=2,
                          reuse_features=True,
                          **dict(stride=1, atom_indices=None))
        feat, desc = f()
        for i in range(3):
            _ = f()



    def test_generate_random_features_reuse(self):
        f = FeatureFeeder(trajectory_file=self.traj,
                          topology_file=self.top,
                          stream=True,
                          n_features=2,
                          reuse_features=False,
                          **dict(stride=1, atom_indices=None))

        feat, desc = f()
        for i in range(3):
            _ = f()



    def test_to_ascii(self):
        f = FeatureFeeder(trajectory_file=self.traj,
                          topology_file=self.top,
                          stream=False,
                          n_features=2,
                          reuse_features=True,
                          **dict(stride=1, atom_indices=None))
        # f.gen_random_feature(seed=10)
        f(seed=10)
        f.to_ascii(self.fname)
        print(f.generated_features)



    def test_from_ascii(self):
        g = FeatureFeeder()
        g.from_ascii(self.fname, keep_header=True)

        for i in range(3):
            _ = g()