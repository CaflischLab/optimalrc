#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 18:22:14 2021

@author: jwidmer
"""
import unittest
import numpy as np

from optimalRC.RC import ReactionCoord, AdaptiveReactionCoord
import optimalRC.BasisFunctions as bf


class Test_Committor(unittest.TestCase):
    def test_Z_H(self):
        dx = 0.002

        RC = ReactionCoord()
        r = np.array([0.8715131 , 0.0 , 0.98904843, 0.75631192, 0.69425401,
                      0.55577742, 0.29051973, 0.90368423, 1.0, 0.42313819])

        zh1 = RC.compute_Z_h(r, weights=None, dx=dx)
        np.testing.assert_array_almost_equal(sum(zh1[0]), 6.486)
        np.testing.assert_array_almost_equal(sum(zh1[1]), 5000.)



    def test_Z_c1(self):
        RC = ReactionCoord()
        r = np.array([0.8715131 , 0.0 , 0.98904843, 0.75631192, 0.69425401,
                      0.55577742, 0.29051973, 0.90368423, 1.0, 0.42313819])
        dx = 0.002
        zc1 = RC.compute_Z_ca(r, a=1, eq=True, dx=dx)
        np.testing.assert_array_almost_equal(sum(zc1[0]), 6.486)
        np.testing.assert_array_almost_equal(sum(zc1[1]), 11.247000000000002)



    def test_maxZ_c1(self):
        r = np.array([0.8715131 , 0.0 , 0.98904843, 0.75631192, 0.69425401,
                      0.55577742, 0.29051973, 0.90368423, 1.0, 0.42313819])
    
        mx = AdaptiveReactionCoord.find_maxzc1(r, Dt=1,
                                               traj_indices=np.ones_like(r),
                                               eq=True)
        print(np.testing.assert_array_almost_equal(mx,
                                                   (0.5558000000000001,
                                                    0.3275223195419992)))



    def test_basis_fn_ry(self):

        x = np.linspace(0, 10, num=5)
        y = np.linspace(5, 6, num=5)
        degree = 2
        basis_fn = bf.basis_poly_ry(x, y, degree)
        ref = np.array([[1.        , 1.        , 1.        , 1.        , 1.        ],
                        [0.83333333, 0.875     , 0.91666667, 0.95833333, 1.        ],
                        [0.69444444, 0.765625  , 0.84027778, 0.91840278, 1.        ],
                        [0.        , 0.25      , 0.5       , 0.75      , 1.        ],
                        [0.        , 0.21875   , 0.45833333, 0.71875   , 1.        ],
                        [0.        , 0.0625    , 0.25      , 0.5625    , 1.        ]])
        np.testing.assert_array_almost_equal(basis_fn, ref)



    def test_basis_fn_ry_fenv(self):
        """Basis function f(r,y) with fenv"""
        x = np.linspace(0, 10, num=5)
        y = np.linspace(5, 6, num=5)
        degree = 2
        r0 = 3.8
        scale = 0.53585
        fenv = bf.laplace_distribution(x, r0, scale=scale)
        fenv_ref = np.array([1.18580600e-168, 3.56152705e-058, 9.34848317e-054,
                             3.11256585e-164, 1.03632493e-274])
        np.testing.assert_array_almost_equal(fenv, fenv_ref)

        basis_fn = bf.basis_poly_ry(x, y, degree, fenv=fenv)
        ref = np.array([[1.18580600e-168, 3.56152705e-058, 9.34848317e-054,
                         3.11256585e-164, 1.03632493e-274],
                        [9.88171668e-169, 3.11633616e-058, 8.56944291e-054,
                         2.98287561e-164, 1.03632493e-274],
                        [8.23476390e-169, 2.72679414e-058, 7.85532267e-054,
                         2.85858912e-164, 1.03632493e-274],
                        [0.00000000e+000, 8.90381761e-059, 4.67424159e-054,
                         2.33442439e-164, 1.03632493e-274],
                        [0.00000000e+000, 7.79084041e-059, 4.28472146e-054,
                         2.23715670e-164, 1.03632493e-274],
                        [0.00000000e+000, 2.22595440e-059, 2.33712079e-054,
                         1.75081829e-164, 1.03632493e-274]])
        np.testing.assert_array_almost_equal(basis_fn, ref)



    def test_basis_fn_r(self):
        """Basis function f(r)"""
        x = np.linspace(0, 10, num=5)
        degree = 2
        basis_fn = bf.basis_poly_r(x, degree)
        ref = np.array([[1.    , 1.    , 1.    , 1.    , 1.    ],
                        [0.    , 0.25  , 0.5   , 0.75  , 1.    ],
                        [0.    , 0.0625, 0.25  , 0.5625, 1.    ]])
        np.testing.assert_array_almost_equal(basis_fn, ref)



    def test_update_r(self):
        """Update r with f(r)"""
        RC = ReactionCoord()
        r = np.array([0.15599452, 0, 0.05808361, 0.86617615, 0.60111501, 1, 0.70807258])
        degree = 3
        basis_fn = bf.basis_poly_r(r, degree)
        ri_is_boundary = np.array([0., 1., 0., 0., 0., 1., 0.])
        basis_fn_b, coeffs = RC.calculate_optimal_coeffs(r,
                                                         basis_fn,
                                                         ri_is_boundary,
                                                         np.ones(r.shape, np.float64))
        updated_r = RC.update_r(r, basis_fn_b, coeffs)
        ref_r = np.array([0.1690375 , 0.        , 0.35496794, 0.45839604, 0.57849615,
                          1.        , 0.64503206])
        np.testing.assert_array_almost_equal(updated_r, ref_r)



    def test_update_r_nonEq(self):
        """Update r with f(r)"""
        RC = ReactionCoord()
        r = np.array([0.15599452, 0, 0.05808361, 0.86617615, 0.60111501, 1, 0.70807258])
        degree = 3
        basis_fn = bf.basis_poly_r(r, degree)
        ri_is_boundary = np.array([0., 1., 0., 0., 0., 1., 0.])
        traj_breaks = [0, 0, 0, 1, 1, 1, 1]
        basis_fn_b, coeffs = RC.calculate_optimal_coeffs(r,
                                                         basis_fn,
                                                         ri_is_boundary,
                                                         traj_breaks)
        updated_r = RC.update_r(r, basis_fn_b, coeffs)
        ref_r = np.array([0.99819865, 0.        , 0.98876586,
                          1.        , 1.        , 1.        , 0.99819865])
        np.testing.assert_array_almost_equal(updated_r, ref_r)



    def test_update_r_self(self):
        # Compare r after one step of non-adaptive optimization
        deg = 4
        deg_r = 16

        RC = ReactionCoord()
        ARC = AdaptiveReactionCoord()
        r = np.array([0.8715131 , 0.7815385 , 0., 0.75631192, 1.,
                      0.55577742, 0.29051973, 1, 0.02850217, 0.42313819])
        RC.initialize_RC(r)
        ARC.initialize_RC(r)
        y = np.array([0.72914283, 0.970247  , 0.8768575 , 0.05000691, 0.08268756,
                      0.00826425, 0.87830112, 0.41367818, 0.56675217, 0.04696322])

        feat, desc = (y, 'feature')
        RC(degree=deg, new_feature=feat, description=desc, optimize_r_degree=deg_r)
        ARC(degree=deg, new_feature=feat, description=desc, optimize_r_degree=deg_r)

        np.testing.assert_array_almost_equal(RC.r, ARC.r)
    
    
    
    def test_update_spatial(self):
        deg = deg_r = 1
        adapt_degree = 2
        ARC = AdaptiveReactionCoord(adaptive_interval=2, spatial_tol=0.03)
        r = np.array([0.8715131 , 0.7815385 , 0., 0.75631192, 1.,
                      0.55577742, 0.29051973, 1, 0.02850217, 0.42313819])
        ARC.initialize_RC(r)
        y = np.array([0.72914283, 0.970247  , 0.8768575 , 0.05000691, 0.08268756,
                      0.00826425, 0.87830112, 0.41367818, 0.56675217, 0.04696322])

        feat, desc = (y, 'feature')
        for _ in range(3):
            ARC(degree=deg, new_feature=feat, description=desc,
                optimize_r_degree=deg_r, adapt_degree=adapt_degree)



    def test_to_natural(self):
        r = np.array([0.8715131 , 0.7815385 , 0., 0.75631192, 1.,
              0.55577742, 0.29051973, 1, 0.02850217, 0.42313819])
        RC = ReactionCoord()
        q_tilde = RC.to_natural(r, dx=0.002)
        ref = np.array([15.53230416, 13.7184258 ,  0.        , 13.27018056, 18.17157937,
                        9.95685458,  5.58250412, 18.17157937,  0.72328527,  7.71025216])
        np.testing.assert_array_almost_equal(q_tilde, ref)



    def test_Dr2_single_traj(self):
        r = np.array([0., 1., 2., 4., 0.])
        RC = ReactionCoord()
        Dr_squared = RC.evaluate_Dr2(r)
        assert Dr_squared == 22



    def test_Dr2_nonEq(self):
        r = np.array([0., 1., 2., 4., 0.])
        RC = ReactionCoord()
        RC.initialize_RC(r, traj_indices=[0, 0, 0, 1, 1])
        Dr_squared = RC.evaluate_Dr2(r, RC.traj_breaks)
        assert Dr_squared == 18



    def test_count_states(self):
        RC = ReactionCoord()
        states_AB = np.array([0, 0.5, 1, 1, 0, 1, 0.5, 1, 0])
        traj_indices = [1, 1, 1, 1, 1, 0, 0, 0, 0]
        n_AB = RC.count_transitions_AB(states_AB, traj_indices)
        assert n_AB == (1, 2, 3)
        traj_indices = [1, 1, 1, 1, 1, 1, 1, 1, 1]
        n_AB = RC.count_transitions_AB(states_AB, traj_indices)
        assert n_AB == (2, 2, 4)
        states_AB = np.array([0, 0, 1])
        n_AB = RC.count_transitions_AB(states_AB)
        assert n_AB == (1, 0, 1)
        states_AB = np.array([0, 0, 0])
        n_AB = RC.count_transitions_AB(states_AB)
        assert n_AB == (0, 0, 0)
    