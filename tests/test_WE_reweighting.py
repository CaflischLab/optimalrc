#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 10:33:35 2022

@author: jwidmer
"""
import pytest
import unittest
import numpy as np

import optimalRC.WeightedEnsemble_reweighting as we

class Test_WE_reweighting(unittest.TestCase):
    
    @staticmethod
    def test_pnorm_distance():
        feature = np.array([[[0., 0], [2., 0.]]]).reshape(2, -1)
        n_distances = we.pnorm_distance(feature, p=1)
        assert (n_distances.T == n_distances).all()
        assert n_distances[1, 0] == 2.
        n_distances = we.pnorm_distance(feature, p=2)
        assert n_distances[1, 0] == 4.


    @staticmethod
    def test_periodic_distance():
        # Assume radians; 3.1 should be closer to -3 than to 0
        feature = np.array([6, 0.01, 3]).reshape(3, -1)
        # feature = np.array([-3.1, 3.1, 0]).reshape(3, -1)
        n_distances = we.periodic_distance(feature, p=1)

        assert (n_distances.T == n_distances).all()
        # On a circle, 6 is closer to 0.01 than 3
        assert (np.argsort(n_distances[1]) == [1, 0, 2]).all()
        n_distances = we.periodic_distance(feature, p=2)
        assert n_distances[1, 2] == 2.99


    @staticmethod
    def test_absorb_weights():
        weights = np.array([1/4, 1/4, 1/4, 1/4])
        replicas_terminated = np.array([0])
        untouched_replicas = np.array([2, 3])
        feature = np.array([[0.2, 0.8, 0.3, 0.21]]).reshape(4, -1)
    
        w = we.absorb_weights(weights, replicas_terminated,
                             untouched_replicas, feature, 1, 1)
        assert np.allclose(w, [1/4, 1/4, 1/4, 2/4])
    
    
    @staticmethod
    def test_split_weights():
        reseeding = np.array([1, 1, 2, 3])
        repicas_duplicated = np.array([1])
        w = we.split_weights(np.array([1/4, 1/4, 1/4, 2/4]),
                             repicas_duplicated, reseeding)
        ref = [0.125, 0.125, 0.25 , 0.5  ]
        assert np.allclose(w, ref)
    
    
    @staticmethod
    def test_find_weights_basic():
        trace = np.array([[1, 1, 2, 3],
                          [0, 2, 2, 3]])
        features = np.array([[0.2, 0.8, 0.3, 0.5],
                              [0.6, 0.5, 1.2, 0.51]])
        weight_matrix = we.find_weights(trace, features, n=1, p=1)
        ref = [[0.25 , 0.25 , 0.25 , 0.25 ],
               [0.125, 0.125, 0.5  , 0.25 ],
               [0.125, 0.25 , 0.25 , 0.375]]
        assert np.allclose(weight_matrix, ref)
    
    
    @staticmethod
    def test_find_weights_1Dfeature_n2():
        trace = np.array([[1, 1, 2, 3],
                          [0, 2, 2, 3]])
        features = np.array([[0.2, 0.8, 0.3, 0.5],
                              [0.6, 0.5, 1.2, 0.51]])
        weight_matrix = we.find_weights(trace, features, n=2, p=1)
        ref = [[0.25  , 0.25  , 0.25  , 0.25  ],
               [0.125 , 0.125 , 0.375 , 0.375 ],
               [0.1875, 0.1875, 0.1875, 0.4375]]
        assert np.allclose(weight_matrix, ref)
    
    
    @staticmethod
    def test_find_weights_1Dfeature_n2_expkernel():
        trace = np.array([[1, 1, 2, 3],
                          [0, 2, 2, 3]])
        features = np.array([[0.2, 0.8, 0.3, 0.5],
                              [0.6, 0.5, 1.2, 0.51]])
        weight_matrix = we.find_weights(trace, features, n=2, p=1,
                                        kernel=we.RBF, gamma=1)
        ref = [[0.25      , 0.25      , 0.25      , 0.25      ],
               [0.125     , 0.125     , 0.3874585 , 0.3625415 ],
               [0.1846894 , 0.19372925, 0.19372925, 0.4278521 ]]
        assert np.allclose(weight_matrix, ref)
    
    
    @staticmethod
    def test_find_weights_2Dfeature():
        trace = np.array([[1, 1, 2]])
        features = np.array([[[1., 1.], [2, 2], [0.5, 1.5]]])
        weight_matrix = we.find_weights(trace, features, n=1, p=1)
        ref = [[1/3, 1/3,1/3],
               [1/6, 1/6, 4/6]]
        assert np.allclose(weight_matrix, ref)
        features = np.array([[[1., 1.], [0.5, 1.5], [2, 2]]])
        weight_matrix = we.find_weights(trace, features, n=1, p=1)
        assert np.allclose(weight_matrix[0], weight_matrix[1])
    
    
    @staticmethod
    def test_find_weights_toohighN():
        trace = np.array([[0, 1, 1]])
        features = np.ones_like(trace)
        weight_matrix = we.find_weights(trace, features, n=2, p=2, zscore=False)
        ref = [[0.33333333, 0.33333333, 0.33333333],
               [0.5, 0.25, 0.25]]
        assert np.allclose(ref, weight_matrix)
    
    
    @staticmethod
    def test_find_weights_realistic():
        features = np.array([[ 
        9.13811,  9.65802,  9.55008,  9.85871,  9.67009,  9.78535,
        9.7142 ,  9.7088 ,  9.53571,  9.91267,  9.83347,  9.70038,
        9.59094,  9.63077,  9.66057,  9.75674,  9.72714,  9.46079,
        9.68391,  8.8482 ,  9.79376,  9.7273 ,  9.46511,  9.67683,
        9.51758,  9.75563,  9.92945, 10.0175 ,  9.34932,  9.88007,
        9.60738,  9.15179]])
        trace = np.array([[ 0, 18, 22,  3,  4,  5,  6,  7,  8,  9, 18, 11, 12, 13,
                            22, 24, 22, 4, 18,  0, 20, 21, 22, 23, 24, 25, 24, 27,
                            28, 29, 30, 31]])
        weight_matrix = we.find_weights(trace, features, n=1, p=1)
        # Check if weight of 24 is split in 3 (15, 24, 26)
        assert weight_matrix[1, 24] == 1/3 * weight_matrix[0, 0]
        # Check if weight of 3 is doubled after absorbing replica 10
        assert weight_matrix[1, 3] == 2 * weight_matrix[0, 0]


    @staticmethod
    def test_zscore():
        features = np.array([[0.2, 0.8, 0.3, 0.5],
                             [0.6, 0.5, 1.2, 0.51]])
        we.calc_zscore(features, n_frames=2, n_reps=4)

    @staticmethod
    def test_zscore_multiD_features():
        features = np.array([
                            [[0.2, 0.8], [0.3, 0.5]],
                            [[0.2, 0.8], [0.5, 0.4]],
                            [[0.6, 0.5], [1.2, 0.1]]
                             ])
        we.calc_zscore(features, n_frames=3, n_reps=2)
    
    
    @staticmethod
    def test_zscore_0variance():
        m = r'Variance in columns [0] is 0'
        with pytest.raises(ZeroDivisionError) as exc_info:
                we.calc_zscore(np.ones((3, 2)), 3, 2)
        
        assert m == exc_info.value.args[0]
    
    
    @staticmethod
    def test_RBF_kernel():
        feature = np.array([[0.2, 1, 0.4, 0.5]]).reshape(4, -1)
        n_distances = we.pnorm_distance(feature, p=1)[3, :3]
        ref = [0., 0., 1.]
        assert np.allclose(we.RBF(n_distances, gamma=1e3), ref)
        ref = [0.3333, 0.3333, 0.3333]
        assert np.allclose(we.RBF(n_distances, gamma=1e-5), ref, rtol=1.e-3)


