
## Unsupervised variational optimization of reaction coordinate

Following http://dx.doi.org/10.1063/1.4935180, http://dx.doi.org/10.1021/acs.jctc.8b00101 and https://doi.org/10.1021/acs.jctc.1c00218. 
Code is adapted from the associated repository https://github.com/krivovsv/NPNE.
Tensorflow is used for parallelization, but has been tested only for use with CPUs.

### Usage

Functionalities are split between two categories; the class `FeatureFeeder` is used to read features from trajectory files or an ASCII-file. In the former case, it is based on the `MDtraj` package along with the corresponding options, e.g. to read only certain atoms, streaming, and the stride size.

The class `ReactionCoord` and its derivative `AdaptiveReactionCoord` are initialized with an array with each element in [0, 1], where 0 and 1 mark the boundaries. Then, upon each call, a provided feature is used to update the reaction coordinate using polynomial basis functions of the specified degree to progressively approximate the committor function. In addition, various cut functions can be calculated.

An template script for optimization is included.